# Notes on Android Debugging

If you are reading this you are probably interested in developing Cwtch for Android! Awesome.

The Cwtch UI app is intended to be a single codebase that runs on multiple platforms. This
complicates the build process in favour of simplifying the code (so goes the theory).

We make use of https://github.com/therecipe/qt/ for deploying Go/Qt code to Android. Before you venture into the weeds
of this README please take a look at the [Installation](https://github.com/therecipe/qt/wiki/Installation) 
and [Setup instructions](https://github.com/therecipe/qt/wiki/Deploying-Linux-to-Android) in therecipe/qt.

## Building

Check out and follow the instructions at https://github.com/therecipe/qt/wiki/Deploying-Linux-to-Android as they are sufficient,
below you will find high-level notes regarding the process.

You need to run `qtsetup --qt_version=<vesion> full android` for the non-docker setup. You will need to do this
for every major version change of therecipe dependencies.

You will also need the Android 28 SDK (Pie), the NDK, SDK build tools and platform tools, gradle and **JDK 8**

    JAVA_JDK=/path/to/jre8
    ANDROID_NDK_DIR=/path/to/ndk

Once all that setup is done you should be able to run:

    ANDROID_MODULES_INCLUDE="Core,Gui,Svg,QuickWidgets,Xml" qtdeploy build android 

2-4 minutes later an android apk will pop out in `./deploy/android/build-debug.apk`.

### Build Setup Issues we have seen 

* `Could not determine java version from <blah>` - this is thrown by gradle inside the `androiddeployqt` process when the
Java version is *not* JRE8. Ensure that JAVA_HOME is pointed to the correct java installation.
* ` readelf <blah> "is not an ordinary file"` - this isn't actually an error that will stop the build, but sometimes
because of the very long debug log output you will come across it when trying to find the *actual* error (which is
probably a Java version issue). It can be safely ignored.
* `could not find QAndroid...` / `CPP build errors` - you will need to run `qtsetup` full android` for the Qt version
you are using.
    * Example: androidextras_android.cpp:9:10: fatal error: 'QAndroidActivityResultReceiver' file not found

## Testing on a Real Device

Consult the Android documentation on setting up your device for development.

You will need an android sdk, setup your device for USB Debugging and then with `adb` you can do:

    adb install -r ./deploy/android/build-debug.apk
 
 To get the logs you can run

    adb logcat
    
Android Studio provides a nice logcat interface for quickly filtering log files that can be very useful when trying to
debug complex behavior, but command line tools like `grep` and the built-in [logcat filtering](https://developer.android.com/studio/command-line/logcat)
 should also suffice.
 
 *Important*: Cwtch UI technically runs *3* different applications: Cwtch Frontend (application client), 
 Cwtch Backend (application server) and Tor. When filtering logcat you should be aware that some of your messages might
 be getting logged by a different process.
 
 (*Ctrl-F Helper: "Why are log messages missing"*) 

# Bundled Libraries

There seems to be a bug in Qt (https://bugreports.qt.io/browse/QTBUG-84371) that prevents the use of
`AndroidExtras` in `ANDROID_MODULES_INCLUDE` so we bundle it in `android/libQt5AndroidExtras.so` along with
`libtor` for Tor support.

## Non-SDK Interfaces

e.g. java.lang.NoSuchFieldException: No field mPivotX in class Landroid/graphics/drawable/RotateDrawable$RotateState;

* https://bugreports.qt.io/browse/QTBUG-71590

## Plugins

Theoretically speaking it should be possible to use `ANDROID_EXTRA_PLUGINS` to include support for e.g.
SVG images on Android. However, we have been unable to make it work. If you would like to try, the following
issues might be helpful:

* https://bugreports.qt.io/browse/QTBUG-60022

## Notifications

- Android 8 (API Level 26) forces you to call setChannelId() 
- Android 9 "Do Not Disturb" mode also hides all notifications 
- Setting up notification channels only seems possible *once* per install. any changes you need to make
require that the app is reinstalled, or the actual channel deleted and changed.
