.PHONY: all clean linux windows android

DEFAULT_GOAL: linux

SHELL  := env QT_BUILD_VERSION=$(QT_BUILD_VERSION) $(SHELL)
QT_BUILD_VERSION ?= "5.13.4"

all: clean linux windows android

clean:
	rm -r vendor || true
	find -type f -iname "moc*" | xargs rm
	find -iname "rcc*" | xargs rm

linux:
	date
	qtdeploy -qt_version $(QT_BUILD_VERSION) build linux 2>&1 | tee qtdeploy.log | pv
	date
	cp -R assets deploy/linux/

windows:
	date
	qtdeploy -qt_version $(QT_BUILD_VERSION) build windows 2>&1 | tee qtdeploy.log | pv
	date
	cp -R assets deploy/windows/

android:
	cp -R assets android/
	date
	## TODO have this also include AndroidExtras (see ANDROID_DEBUGGING) for full notes.
	env ANDROID_MODULES_INCLUDE="Core,Gui,Svg,QuickWidgets,Xml" qtdeploy -debug -qt_version $(QT_BUILD_VERSION) build android 2>&1 | tee qtdeploy.log | pv
	date

