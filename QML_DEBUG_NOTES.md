# Settings List / Flickable

Content not scrolling: Flickable does some reparenting behind the scenes and so
in the top level child of the Flickable you will need:

    parent: root.contentItem
    
And in the flickable you will need to set the contentHeight:

    contentHeight: <childId>.height + <padding>