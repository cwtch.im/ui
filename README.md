# Cwtch - UI
 
This codebase provides a graphical user interface for Desktop and Android for [Cwtch: Privacy Preserving Infrastructure for Asynchronous, Decentralized and Metadata Resistant Applications](https://git.openprivacy.ca/cwtch.im/cwtch)

# Security

**Cwtch is an experimental concept and prototype. We do not recommend you use Cwtch today if you require secure communication.** At least, not yet.
 
 If you discover a security issue, please log an issue above, or email team@cwtch.im.

# Running

# Compiling
The UI is built using QT so you will need the development libraries and tools for your OS. Currently we are using QT 5.13.0

This code relies on [therecipe/qt](https://github.com/therecipe/qt) before getting started consult the [Installation](https://github.com/therecipe/qt/wiki/Installation) and [Getting Started](https://github.com/therecipe/qt/wiki/Getting-Started) documentation to get that up and running. It will make building this much easier.

Cwtch UI uses the Go module system for dependancies, and git submodules for includin QML components

```
git submodule init
git submodule update
```

## Linux 

    go mod vendor
    qtdeploy build linux
    ./deploy/linux/ui -local -debug 2>&1 | grep -v 'Detected anchors on an item that is managed by a layout.'

The -debug flag increases the logging level.

The -local flag means the binary should use the local copies of QML files instead of the compiled ones allowing for more rapid UI development. 

The grep statement filters out some QML noise.

## Android

We supply an arm-pie version of tor in `android/libs/armeabi-v7a` with the name `libtor.so`

    go mod vendor
    qtdeploy -docker build android
    adb install deploy/android/build-debug.apk

## Windows

- Download and install [QT](https://www.qt.io/download) Open Source edition
- Go to My Computer - Right Click on This PC - Advanced System Settings -  Environment Variables
    - New - QT_DIR = C:\Qt (or wherever you have it installed)
    - New - QT_VERSION = 5.12.1 (because I picked 5.12.1)
    - Edit the Path Variable and Add C:\Qt\Tools\mingw530_32\bin
- Install [MSYS2](https://www.msys2.org/) (Bash shell for windows)
- Install [Go](https://golang.org/) and make sure it's in the PATH, and add your GOPATH/bin to the PATH
- Setup therecipe/qt (instructions https://github.com/therecipe/qt/wiki/Installation-on-Windows)
    - `go get -u -v github.com/therecipe/qt/cmd/...`
    - `qtsetup` (Can take around an hour?)

If all that is done, then check out cwtch.im/ui

    go mod vendor
    qtdeploy
    deploy/windows/ui

Currently if you do not have Tor running Cwtch will start tor for you, which will open a dos console window. We are working to hide this in the future.

# Contributing

## Translations

(If you'd like to add a new translation, feel free to open an issue and we can generate the .ts file for you to edit by hand, or in QT Linguist!)

### Add new translation

0. Install the language tools if you have not already (on linux: `sudo apt-get install qttools5-dev qttools-dev-tools`)

1. Edit `ui.pro` and add a new line in the variable definition for `TRANSLATIONS` like:

    TRANSLATIONS = i18n/translation_en.ts \
                i18n/translation_de.ts

2. Edit `qml.qrc` and add a new line near the other translation files like:

    \<file>i18n/translation_de.qm\</file>

3. To update and generate your new translation file, run `lupdate ui.pro` and you will find your new file in the `i18n/` directory.

### Updating translations

Run `lupdate ui.pro` to update all the translation files in the `i18n/` directory, then edit your languages' .ts file to make the appropriate changes. It is recommended to use QT Linguist (`linguist`) to edit the files.

When done, run `lrelease ui.pro` to compile all the translations from their .ts files into .qm files for use in the program. Then you can compile the ui program.

