package ca.openprivacy.cwtch.ui;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.NotificationChannel;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.os.Bundle;
import android.content.ComponentName;

import static android.app.Notification.DEFAULT_LIGHTS;
import static android.app.Notification.DEFAULT_SOUND;
import static android.app.Notification.DEFAULT_VIBRATE;
import static android.app.NotificationManager.IMPORTANCE_DEFAULT;
import static android.app.NotificationManager.IMPORTANCE_LOW;
import static android.content.Context.NOTIFICATION_SERVICE;
import static android.content.Intent.FLAG_ACTIVITY_CLEAR_TOP;
import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;
import static android.os.Build.VERSION.SDK_INT;
import static android.app.Notification.CATEGORY_SERVICE;

public class CwtchActivity extends org.qtproject.qt5.android.bindings.QtActivity
{
    private static NotificationManager m_notificationManager;
    private static Notification.Builder m_builderOngoing;
    private static CwtchActivity m_instance;

    private static String NOTIFICATION_CHANNEL_ID = "cwtch_notification_channel";
    private static int CONTENT_NOTIFICATION_ID = 2;
    private static String CONTENT_NOTIFICATION_ID_NAME = "Notifications from Peers";



    public CwtchActivity() {
        m_instance = this;
    }

    // https://github.com/bbernhard/qtandroidservices_example/blob/master/source/java/MyCustomAppActivity.java
    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        Log.i("CwtchActivity", "Starting service!");
        Intent serviceIntent = new Intent(this, ca.openprivacy.cwtch.ui.CwtchService.class);
        ComponentName ret = startService(serviceIntent);
        if (ret == null) {
            Log.i("CwtchActivity", "Started Service: FAILED with null");
        } else {
            Log.i("CwtchActivity", "Started Service: " + ret.flattenToString());
        }
    }

    public static void notify(String s, String o)
    {
        if (m_notificationManager == null) {
            m_notificationManager = (NotificationManager)m_instance.getSystemService(Context.NOTIFICATION_SERVICE);
            createNotificationChannel();
        }

        // Apparently thr android documentation is just wrong and we need to provide a setGroupSummary
        // notification regardless of targetted support version...
         Notification groupSummary =
            new Notification.Builder(m_instance)
                    .setContentTitle("Cwtch")
                    .setContentText("New Message from Peer: " + o)
                    .setGroupSummary(true)
                    .setWhen(System.currentTimeMillis())
                    .setSmallIcon(R.drawable.ic_launcher)
                    .setGroup(NOTIFICATION_CHANNEL_ID)
                    .setChannelId(NOTIFICATION_CHANNEL_ID)
                    .build();
        m_notificationManager.notify(1, groupSummary);

        Notification.Builder m_builder = new Notification.Builder(m_instance)
            .setSmallIcon(R.drawable.ic_launcher)
            .setChannelId(NOTIFICATION_CHANNEL_ID)
            .setGroup(NOTIFICATION_CHANNEL_ID)
            .setWhen(System.currentTimeMillis())
            .setAutoCancel(true)
            .setContentTitle("New Message from Peer: " + o)
            .setContentText("[redacted:  Open Cwtch App to see the Message]");
        m_notificationManager.notify(CONTENT_NOTIFICATION_ID++, m_builder.build());



    }

    private static void createNotificationChannel() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (SDK_INT >= 26) {
            String description = "Cwtch Notification Channel";
            NotificationChannel channel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, CONTENT_NOTIFICATION_ID_NAME, NotificationManager.IMPORTANCE_HIGH);
            channel.setDescription(description);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            m_notificationManager.createNotificationChannel(channel);

        }
    }

    // handle root level back button push as a home button push: don't exit, just go to home screen
    public static void rootHomeHandle() {
        Intent i = new Intent(Intent.ACTION_MAIN);
        i.addCategory(Intent.CATEGORY_HOME);
        m_instance.startActivity(i);
    }
}
