#!/bin/sh

docker pull registry.openprivacy.ca:5000/therecipe/qt:linux_static-2019.11
docker tag 572cd474b221 therecipe/qt:linux_static
docker rmi registry.openprivacy.ca:5000/therecipe/qt:linux_static-2019.11

docker pull registry.openprivacy.ca:5000/therecipe/qt:windows_64_static-2019.11
docker tag 54c984171442 therecipe/qt:windows_64_static
docker rmi registry.openprivacy.ca:5000/therecipe/qt:windows_64_static-2019.11

docker pull registry.openprivacy.ca:5000/therecipe/qt:android-2019.11
docker tag 4d4b5415b19d therecipe/qt:android
docker rmi registry.openprivacy.ca:5000/therecipe/qt:android-2019.11
