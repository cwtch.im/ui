module cwtch.im/ui

go 1.12

require (
	cwtch.im/cwtch v0.5.0
	git.openprivacy.ca/openprivacy/connectivity v1.3.3
	git.openprivacy.ca/openprivacy/log v1.0.2
	github.com/c-bata/go-prompt v0.2.3 // indirect
	github.com/google/go-cmp v0.4.0 // indirect
	github.com/gopherjs/gopherjs v0.0.0-20200209183636-89e6cbcd0b6d // indirect
	github.com/mattn/go-colorable v0.1.6 // indirect
	github.com/mattn/go-runewidth v0.0.9 // indirect
	github.com/mattn/go-tty v0.0.3 // indirect
	github.com/pkg/term v0.0.0-20190109203006-aa71e9d9e942 // indirect
	github.com/therecipe/qt v0.0.0-20200126204426-5074eb6d8c41
	github.com/therecipe/qt/internal/binding/files/docs/5.12.0 v0.0.0-20200126204426-5074eb6d8c41 // indirect
	github.com/therecipe/qt/internal/binding/files/docs/5.13.0 v0.0.0-20200126204426-5074eb6d8c41 // indirect
	go.etcd.io/bbolt v1.3.5 // indirect
	golang.org/x/crypto v0.0.0-20201016220609-9e8e0b390897 // indirect
	golang.org/x/net v0.0.0-20201022231255-08b38378de70 // indirect
	golang.org/x/sys v0.0.0-20201022201747-fb209a7c41cd // indirect
)