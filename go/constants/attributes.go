package constants

const SchemaVersion = "schemaVersion"

const Name = "name"
const LastRead = "last-read"
const Picture = "picture"
const ShowBlocked = "show-blocked"
const UnreadMsgCount = "unread-message-count"
const ApprovedTime = "approved-time"

const ProfileTypeV1DefaultPassword = "v1-defaultPassword"
const ProfileTypeV1Password = "v1-userPassword"

// PeerOnline stores state on if the peer believes it is online
const PeerOnline = "peer-online"

const StateProfilePane = "state-profile-pane"
const StateSelectedConversation = "state-selected-conversation"
const StateSelectedProfileTime = "state-selected-profile-time"
