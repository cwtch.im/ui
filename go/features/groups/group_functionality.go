package groups

import (
	"cwtch.im/ui/go/the"
	"encoding/base64"
	"errors"
	"fmt"
	"git.openprivacy.ca/openprivacy/log"
	"strings"
)

const ServerPrefix = "server:"
const TofuBundlePrefix = "tofubundle:"
const GroupPrefix = "torv3"
const GroupExperiment = "tapir-groups-experiment"

type GroupFunctionality struct {
}

// ExperimentGate returns GroupFunctionality if the experiment is enabled, and an error otherwise.
func ExperimentGate(experimentMap map[string]bool) (*GroupFunctionality, error) {
	if experimentMap[GroupExperiment] {
		return new(GroupFunctionality), nil
	}
	return nil, fmt.Errorf("gated by %v", GroupExperiment)
}

func (gf *GroupFunctionality) SendMessage(handle string, message string) error {
	// TODO this auto accepting behaviour needs some thinking through
	if !the.Peer.GetGroup(handle).Accepted {
		err := the.Peer.AcceptInvite(handle)
		if err != nil {
			log.Errorf("tried to mark a nonexistent group as existed. bad!")
			return err
		}
	}

	_, err := the.Peer.SendMessageToGroupTracked(handle, message)

	return err
}

// ValidPrefix returns true if an import string contains a prefix that indicates it contains information about a
// server or a group
func (gf *GroupFunctionality) ValidPrefix(importString string) bool {
	return strings.HasPrefix(importString, TofuBundlePrefix) || strings.HasPrefix(importString, ServerPrefix) || strings.HasPrefix(importString, GroupPrefix)
}

// HandleImportString handles import strings for groups and servers
func (gf *GroupFunctionality) HandleImportString(importString string) error {

	if strings.HasPrefix(importString, TofuBundlePrefix) {
		bundle := strings.Split(importString, "||")
		gf.HandleImportString(bundle[0][11:])
		gf.HandleImportString(bundle[1])
		return nil
	}

	// Server Key Bundles are prefixed with
	if strings.HasPrefix(importString, ServerPrefix) {
		bundle, err := base64.StdEncoding.DecodeString(importString[7:])
		if err == nil {
			return the.Peer.AddServer(string(bundle))
		}
		return err
	}

	//eg: torv3JFDWkXExBsZLkjvfkkuAxHsiLGZBk0bvoeJID9ItYnU=EsEBCiBhOWJhZDU1OTQ0NWI3YmM2N2YxYTM5YjkzMTNmNTczNRIgpHeNaG+6jy750eDhwLO39UX4f2xs0irK/M3P6mDSYQIaOTJjM2ttb29ibnlnaGoyenc2cHd2N2Q1N3l6bGQ3NTNhdW8zdWdhdWV6enB2ZmFrM2FoYzRiZHlkCiJAdVSSVgsksceIfHe41OJu9ZFHO8Kwv3G6F5OK3Hw4qZ6hn6SiZjtmJlJezoBH0voZlCahOU7jCOg+dsENndZxAA==
	if strings.HasPrefix(importString, GroupPrefix) {
		return the.Peer.ImportGroup(importString)
	}

	return errors.New("invalid_group_invite_prefix")
}
