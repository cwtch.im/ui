package handlers

import (
	"cwtch.im/cwtch/app"
	"cwtch.im/cwtch/app/plugins"
	"cwtch.im/cwtch/event"
	"cwtch.im/cwtch/model/attr"
	"cwtch.im/ui/go/constants"
	"cwtch.im/ui/go/features/groups"
	"cwtch.im/ui/go/the"
	"cwtch.im/ui/go/ui"
	"git.openprivacy.ca/openprivacy/log"
	"os"
	"strconv"
)

func App(gcd *ui.GrandCentralDispatcher, subscribed chan bool, reloadingAccounts bool) {
	q := event.NewQueue()
	the.AppBus.Subscribe(event.NewPeer, q)
	the.AppBus.Subscribe(event.PeerError, q)
	the.AppBus.Subscribe(event.AppError, q)
	the.AppBus.Subscribe(event.ACNStatus, q)
	the.AppBus.Subscribe(event.ReloadDone, q)
	the.AppBus.Subscribe(event.ACNVersion, q)
	subscribed <- true

	the.CwtchApp.QueryACNVersion()

	initialProfileLoad := false
	if !reloadingAccounts {
		initialProfileLoad = true
		the.CwtchApp.LoadProfiles(the.AppPassword)
	}

	var latestProfileSelectedTs int64 = 0
	var latestProfileSelectedOnion = ""

	for {
		e := q.Next()

		switch e.EventType {

		case event.ACNStatus:
			progStr := e.Data[event.Progreess]
			progress, _ := strconv.Atoi(progStr)
			message := e.Data[event.Status]
			var statuscode int
			log.Debugf("ACNStatus: %d %v\n", progress, message)
			if progress >= -2 && progress < 0 {
				statuscode = 0
			} else if progress >= 0 && progress < 50 {
				statuscode = 1
			} else if progress >= 50 && progress < 100 {
				statuscode = 2
			} else {
				statuscode = 3
			}
			gcd.SetTorStatus(statuscode)
		case event.ACNVersion:
			version := e.Data[event.Data]
			gcd.SetTorVersion(version)
		case event.PeerError:
			// current only case
			log.Errorf("couldn't load profiles: %v", e.Data[event.Error])
			os.Exit(1)

		case event.AppError:

			if e.Data[event.Error] == event.AppErrLoaded0 {
				if reloadingAccounts {
					reloadingAccounts = false
				} else if initialProfileLoad {
					initialProfileLoad = false
					gcd.Loaded()
				} else {
					gcd.InvokePopup("0-profiles")
				}
			}

		case event.ReloadDone:
			reloadingAccounts = false

			// profilePane: 3 as per main.qml
			if gcd.GlobalSettings.StateRootPane == 3 && latestProfileSelectedOnion != "" {
				gcd.Broadcast("ResetMessagePane")
				gcd.Broadcast("ResetProfile")
				gcd.SetSelectedProfile(latestProfileSelectedOnion)
				gcd.LoadProfile(latestProfileSelectedOnion)
				gcd.ChangeRootPane(3)
			}

			gcd.Loaded()

			if len(the.CwtchApp.ListPeers()) == 0 {
				initialProfileLoad = true
				the.CwtchApp.LoadProfiles(the.AppPassword)
			}
		case event.NewPeer:
			if initialProfileLoad {
				initialProfileLoad = false
				gcd.Loaded()
			}

			onion := e.Data[event.Identity]
			p := the.CwtchApp.GetPeer(onion)

			if tag, exists := p.GetAttribute(app.AttributeTag); !exists || tag == "" {
				p.SetAttribute(app.AttributeTag, constants.ProfileTypeV1DefaultPassword)
			}

			if e.Data[event.Created] == event.True {
				p.SetAttribute(attr.GetPublicScope(constants.Name), p.GetName())
				p.SetAttribute(attr.GetPublicScope(constants.Picture), ui.ImageToString(ui.NewImage(ui.RandomProfileImage(onion), ui.TypeImageDistro)))
			}

			if e.Data[event.Status] != event.StorageRunning || e.Data[event.Created] == event.True {
				p.SetAttribute(attr.GetLocalScope(constants.PeerOnline), event.False)
				the.CwtchApp.AddPeerPlugin(onion, plugins.CONNECTIONRETRY)
				the.CwtchApp.AddPeerPlugin(onion, plugins.NETWORKCHECK)
			}

			log.Debugf("NewPeer for %v\n", onion)
			ui.AddProfile(gcd, onion)

			incSubscribed := make(chan bool)
			go PeerHandler(onion, gcd.GetUiManager(p.GetOnion()), incSubscribed)
			<-incSubscribed

			// TODO: wait till ACN is 100 and online
			if e.Data[event.Status] != event.StorageRunning || e.Data[event.Created] == event.True {
				p.Listen()
				p.StartPeersConnections()
				if _, err := groups.ExperimentGate(gcd.GlobalSettings.Experiments); err == nil {
					p.StartServerConnections()
				}
			}

			blockUnkownPeers, exists := p.GetAttribute(constants.BlockUnknownPeersSetting)
			if exists && blockUnkownPeers == "true" {
				the.EventBus.Publish(event.NewEvent(event.BlockUnknownPeers, map[event.Field]string{}))
			}

			if reloadingAccounts {
				lastSelectedTime, ok := p.GetAttribute(attr.GetSettingsScope(constants.StateSelectedProfileTime))
				if ok {
					lastSelectedTs, _ := strconv.ParseInt(lastSelectedTime, 10, 64)
					if lastSelectedTs > latestProfileSelectedTs {
						latestProfileSelectedTs = lastSelectedTs
						latestProfileSelectedOnion = onion
					}
				}
			}
		}
	}

}
