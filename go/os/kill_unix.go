// +build unix linux android

package os

import (
	"git.openprivacy.ca/openprivacy/log"
	"os/exec"
	"strconv"
	"strings"
)

// CheckProcessAndKill first executes a process search on the system by the last known pid, if the name of the
// process matches the expected name, then it is killed.
// On unix systems the command "ps" is practically universal and should suffice for this...
func CheckProcessAndKill(pid uint64, processName string) {
	log.Debugf("killing: %v", pid)
	bytes, err := exec.Command("ps", "-p", strconv.Itoa(int(pid)), "-o", "command").Output()
	if err == nil {
		// check for a binary @ "/<process_name>"
		lines := strings.Split(string(bytes), "\n")
		if len(lines) >= 2 && strings.Contains(lines[1], "/"+processName+" ") {
			Kill(pid)
			return
		}
		log.Debugf("pid did not relate to expected process, not killing out of caution")
		return
	}
	// no such process
	log.Debugf("no such process %v", pid)
}

// Kill a process based on pid
func Kill(pid uint64) {
	log.Debugf("killing: %v", pid)
	bytes, err := exec.Command("kill", strconv.Itoa(int(pid))).Output()
	if err == nil {
		log.Debugf("kill %v successful: %s", pid, bytes)
	} else {
		// this might now always succeed
		log.Debugf("could not kill pid: %v %v", pid, err)
	}
	return
}
