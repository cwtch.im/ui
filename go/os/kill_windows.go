// +build windows
package os

import (
	"git.openprivacy.ca/openprivacy/log"
	"os/exec"
	"strconv"
	"strings"
)

// CheckProcessAndKill first executes a process search on the system by the last known pid, if the name of the
// process matches the expected name, then it is killed.
// On windows this uses tasklist...
func CheckProcessAndKill(pid uint64, processName string) {
	log.Debugf("looking up process: %v", pid)
	bytes, err := exec.Command("tasklist", "/fi", "pid eq "+strconv.Itoa(int(pid))).Output()
	if err == nil {
		// Output will be something like this:
		//
		// Image Name                     PID Session Name        Session#    Mem Usage
		// ========================= ======== ================ =========== ============
		// process.exe                  	<PID>   Services                   0      8,936 K
		lines := strings.Split(strings.TrimSpace(string(bytes)), "\n")
		log.Debugf("%v\n", lines)

		// check for "<process>.exe"
		if len(lines) >= 3 && strings.HasPrefix(strings.ToLower(strings.TrimSpace(lines[2])), processName+".exe") {
			Kill(pid)
			return
		}
		log.Debugf("pid did not relate to expected process, not attempting process kill out of caution")
		return
	}
	log.Debugf("error checking process: %v", err)
}

// Kill a process based on pid
func Kill(pid uint64) {
	log.Debugf("killing: %v", pid)
	bytes, err := exec.Command("taskkill", "/F", "/PID", strconv.Itoa(int(pid))).Output()
	if err == nil {
		log.Debugf("kill %v successful: %s", pid, bytes)
	} else {
		// this might now always succeed
		log.Debugf("could not kill pid: %v %v", pid, err)
	}
	return
}
