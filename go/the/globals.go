package the

import (
	"cwtch.im/cwtch/app"
	"cwtch.im/cwtch/event"
	libPeer "cwtch.im/cwtch/peer"
	"cwtch.im/cwtch/storage/v1"
	"git.openprivacy.ca/openprivacy/connectivity"
)

// foundation block of the entire app. critical. never change, only obey
const AppPassword = "be gay do crime"

var CwtchApp app.Application
var CwtchService app.ApplicationService
var EventBus event.Manager
var AppBus event.Manager
var ACN connectivity.ACN
var Peer libPeer.CwtchPeer
var CwtchDir string
var IPCBridge event.IPCBridge
var GlobalSettingsFile v1.FileStore
