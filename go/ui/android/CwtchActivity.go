package android

import (
	"git.openprivacy.ca/openprivacy/log"
	"github.com/therecipe/qt/androidextras"
	"github.com/therecipe/qt/core"
)

type CwtchActivity struct {
	core.QObject

	_ func() `constructor:"init"`

	_ string `property:"channel"`
	_ string `property:"notification"`

	_ func(string) `slot:"updateAndroidNotification"`

	_ func() `signal:"rootHomeButtonHandle,auto"`
}

func (c *CwtchActivity) init() {
	log.Debugln("CwtchActivity.init()")
	c.ConnectNotificationChanged(c.updateAndroidNotification)
}

func (c *CwtchActivity) updateAndroidNotification(n string) {

	var err = androidextras.QAndroidJniObject_CallStaticMethodVoid2Caught(
		"ca/openprivacy/cwtch/ui/CwtchActivity",
		"notify",
		"(Ljava/lang/String;Ljava/lang/String;)V",
		n, c.Channel(),
	)

	if err != nil {
		log.Errorf("Error calling Java CwtchActivity.notify(): %v\n", err.Error())
	}
}

func (c *CwtchActivity) rootHomeButtonHandle() {
	log.Infoln("CwtchActivity.rootHomeButtonHandle()!")
	var err = androidextras.QAndroidJniObject_CallStaticMethodVoid2Caught(
		"ca/openprivacy/cwtch/ui/CwtchActivity",
		"rootHomeHandle",
		"()V",
	)

	if err != nil {
		log.Errorf("Error calling Java CwtchActivity.rootHomeButtonHandle(): %v\n", err.Error())
	}
}
