package ui

import (
	"cwtch.im/cwtch/storage/v1"
	"cwtch.im/ui/go/the"
	"encoding/json"
	"git.openprivacy.ca/openprivacy/log"
	"github.com/therecipe/qt/core"
	"io/ioutil"
	"os"
	"path"
	"runtime"
)

const GlobalSettingsFilename = "ui.globals"
const saltFile = "SALT"

type GlobalSettings struct {
	Zoom               float32
	Locale             string
	Theme              string
	PreviousPid        int64
	ExperimentsEnabled bool
	Experiments        map[string]bool
	StateRootPane      int
}

var DefaultGlobalSettings = GlobalSettings{
	Zoom:               1.9,
	Locale:             "en",
	Theme:              "light",
	PreviousPid:        -1,
	ExperimentsEnabled: false,
	Experiments:        make(map[string]bool),
	StateRootPane:      0,
}

func InitGlobalSettingsFile(directory string, password string) error {
	var key [32]byte
	salt, err := ioutil.ReadFile(path.Join(directory, saltFile))
	if err != nil {
		var newSalt [128]byte
		key, newSalt, err = v1.CreateKeySalt(password)
		if err != nil {
			return err
		}
		os.Mkdir(directory, 0700)
		err := ioutil.WriteFile(path.Join(directory, saltFile), newSalt[:], 0600)
		if err != nil {
			return err
		}
	} else {
		key = v1.CreateKey(password, salt)
	}

	the.GlobalSettingsFile = v1.NewFileStore(directory, GlobalSettingsFilename, key)
	return nil
}

func ReadGlobalSettings() (*GlobalSettings, bool) {
	settings := DefaultGlobalSettings
	if runtime.GOOS == "android" {
		settings.Zoom = 2.9
	}
	settings.Locale = core.QLocale_System().Name()

	settingsBytes, err := the.GlobalSettingsFile.Read()
	if err != nil {
		log.Errorf("Could not read global ui settings: %v\n", err)
		return &settings, true //firstTime = true
	}

	err = json.Unmarshal(settingsBytes, &settings)
	if err != nil {
		log.Errorf("Could not parse global ui settings: %v\n", err)
		return &settings, true //firstTime = true
	}

	log.Debugf("Settings: %v", settings)
	return &settings, false
}

func WriteGlobalSettings(globalSettings *GlobalSettings) {
	bytes, _ := json.Marshal(globalSettings)
	err := the.GlobalSettingsFile.Write(bytes)
	if err != nil {
		log.Errorf("Could not write global ui settings: %v\n", err)
	}
}
