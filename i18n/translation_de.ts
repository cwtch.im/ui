<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de_DE">
<context>
    <name>AddGroupPane</name>
    <message>
        <source>create-group-title</source>
        <translation type="vanished">Gruppe Anlegen</translation>
    </message>
    <message>
        <source>server-label</source>
        <extracomment>Server label</extracomment>
        <translation type="vanished">Server</translation>
    </message>
    <message>
        <source>group-name-label</source>
        <extracomment>Group name label</extracomment>
        <translation type="vanished">Gruppenname</translation>
    </message>
    <message>
        <source>default-group-name</source>
        <extracomment>default suggested group name</extracomment>
        <translation type="vanished">Tolle Gruppe</translation>
    </message>
    <message>
        <source>create-group-btn</source>
        <extracomment>create group button</extracomment>
        <translation type="vanished">Anlegen</translation>
    </message>
</context>
<context>
    <name>AddPeerGroupPane</name>
    <message>
        <location filename="../qml/panes/AddPeerGroupPane.qml" line="51"/>
        <source>profile-oniblon-label</source>
        <extracomment>Send this address to peers you want to connect with</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/panes/AddPeerGroupPane.qml" line="61"/>
        <source>copy-btn</source>
        <translation type="unfinished">Kopieren</translation>
    </message>
    <message>
        <location filename="../qml/panes/AddPeerGroupPane.qml" line="65"/>
        <source>copied-to-clipboard-notification</source>
        <extracomment>notification: copied to clipboard</extracomment>
        <translation type="unfinished">in die Zwischenablage kopiert</translation>
    </message>
    <message>
        <location filename="../qml/panes/AddPeerGroupPane.qml" line="81"/>
        <source>add-peer-tab</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/panes/AddPeerGroupPane.qml" line="81"/>
        <source>create-group-tab</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/panes/AddPeerGroupPane.qml" line="81"/>
        <source>join-group-tab</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/panes/AddPeerGroupPane.qml" line="107"/>
        <source>peer-address</source>
        <extracomment>Address</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/panes/AddPeerGroupPane.qml" line="119"/>
        <source>peer-name</source>
        <extracomment>Name</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/panes/AddPeerGroupPane.qml" line="140"/>
        <source>group-name</source>
        <extracomment>Group Name</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/panes/AddPeerGroupPane.qml" line="150"/>
        <source>server</source>
        <extracomment>Server</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/panes/AddPeerGroupPane.qml" line="177"/>
        <source>group-addr</source>
        <extracomment>Address</extracomment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>BulletinOverlay</name>
    <message>
        <location filename="../qml/overlays/BulletinOverlay.qml" line="215"/>
        <source>new-bulletin-label</source>
        <translation>Neue Meldung</translation>
    </message>
    <message>
        <location filename="../qml/overlays/BulletinOverlay.qml" line="227"/>
        <source>post-new-bulletin-label</source>
        <extracomment>Post a new Bulletin Post</extracomment>
        <translation>Neue Meldung veröffentlichen</translation>
    </message>
    <message>
        <location filename="../qml/overlays/BulletinOverlay.qml" line="234"/>
        <source>title-placeholder</source>
        <extracomment>title place holder text</extracomment>
        <translation>Titel...</translation>
    </message>
</context>
<context>
    <name>ChatOverlay</name>
    <message>
        <location filename="../qml/overlays/ChatOverlay.qml" line="69"/>
        <source>chat-history-default</source>
        <extracomment>This conversation will be deleted when Cwtch is closed! Message history can be enabled per-conversation via the Settings menu in the upper right.</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/overlays/ChatOverlay.qml" line="71"/>
        <source>chat-history-disabled</source>
        <extracomment>Message history is disabled.</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/overlays/ChatOverlay.qml" line="73"/>
        <source>chat-history-enabled</source>
        <extracomment>Message history is enabled.</extracomment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ContactList</name>
    <message>
        <source>paste-address-to-add-contact</source>
        <extracomment>ex: &quot;... paste an address here to add a contact ...&quot;</extracomment>
        <translation type="obsolete">Adresse hier hinzufügen, um einen Kontakt aufzunehmen</translation>
    </message>
    <message>
        <location filename="../qml/widgets/ContactList.qml" line="252"/>
        <source>blocked</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>GroupSettingsPane</name>
    <message>
        <location filename="../qml/panes/GroupSettingsPane.qml" line="45"/>
        <source>server-label</source>
        <translation>Server</translation>
    </message>
    <message>
        <location filename="../qml/panes/GroupSettingsPane.qml" line="50"/>
        <location filename="../qml/panes/GroupSettingsPane.qml" line="68"/>
        <source>copy-btn</source>
        <translation>Kopieren</translation>
    </message>
    <message>
        <location filename="../qml/panes/GroupSettingsPane.qml" line="54"/>
        <location filename="../qml/panes/GroupSettingsPane.qml" line="72"/>
        <source>copied-to-clipboard-notification</source>
        <extracomment>notification: copied to clipboard</extracomment>
        <translation type="unfinished">in die Zwischenablage kopiert</translation>
    </message>
    <message>
        <location filename="../qml/panes/GroupSettingsPane.qml" line="63"/>
        <source>invitation-label</source>
        <translation>Einladung</translation>
    </message>
    <message>
        <location filename="../qml/panes/GroupSettingsPane.qml" line="82"/>
        <source>server-info</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/panes/GroupSettingsPane.qml" line="90"/>
        <source>server-connectivity-connected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/panes/GroupSettingsPane.qml" line="90"/>
        <source>server-connectivity-disconnected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/panes/GroupSettingsPane.qml" line="108"/>
        <source>server-synced</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/panes/GroupSettingsPane.qml" line="108"/>
        <source>server-not-synced</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/panes/GroupSettingsPane.qml" line="124"/>
        <source>view-server-info</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/panes/GroupSettingsPane.qml" line="28"/>
        <source>group-name-label</source>
        <translation>Gruppenname</translation>
    </message>
    <message>
        <location filename="../qml/panes/GroupSettingsPane.qml" line="33"/>
        <source>save-btn</source>
        <translation>Speichern</translation>
    </message>
    <message>
        <source>invite-to-group-label</source>
        <extracomment>Invite someone to the group</extracomment>
        <translation type="vanished">In die Gruppe einladen</translation>
    </message>
    <message>
        <source>invite-btn</source>
        <translation type="vanished">Einladen</translation>
    </message>
    <message>
        <location filename="../qml/panes/GroupSettingsPane.qml" line="144"/>
        <source>delete-btn</source>
        <translation>Löschen</translation>
    </message>
</context>
<context>
    <name>ListOverlay</name>
    <message>
        <source>add-list-item</source>
        <extracomment>Add a New List Item</extracomment>
        <translation type="vanished">Liste hinzufügen</translation>
    </message>
    <message>
        <source>add-new-item</source>
        <extracomment>Add a new item to the list</extracomment>
        <translation type="vanished">Neues Listenelement hinzüfgen</translation>
    </message>
    <message>
        <source>todo-placeholder</source>
        <extracomment>Todo... placeholder text</extracomment>
        <translation type="vanished">noch zu erledigen</translation>
    </message>
    <message>
        <location filename="../qml/overlays/ListOverlay.qml" line="33"/>
        <source>search-list</source>
        <extracomment>ex: &quot;Find...&quot;</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/overlays/ListOverlay.qml" line="63"/>
        <source>peer-not-online</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/overlays/ListOverlay.qml" line="212"/>
        <source>add-list-item-btn</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MembershipOverlay</name>
    <message>
        <location filename="../qml/overlays/MembershipOverlay.qml" line="21"/>
        <source>membership-description</source>
        <extracomment>Below is a list of users who have sent messages to the group. This list may not reflect all users who have access to the group.</extracomment>
        <translation>Unten steht eine Liste der Benutzer, die Nachrichten an die Gruppe gesendet haben.  Möglicherweise enthält diese Benutzerzliste nicht alle, die Zugang zur Gruppe haben.</translation>
    </message>
</context>
<context>
    <name>Message</name>
    <message>
        <location filename="../qml/widgets/Message.qml" line="67"/>
        <source>dm-tooltip</source>
        <extracomment>Click to DM</extracomment>
        <translation>Klicken, um DM zu senden</translation>
    </message>
    <message>
        <location filename="../qml/widgets/Message.qml" line="189"/>
        <source>could-not-send-msg-error</source>
        <extracomment>Could not send this message</extracomment>
        <translation>Nachricht konnte nicht gesendet werden</translation>
    </message>
    <message>
        <location filename="../qml/widgets/Message.qml" line="189"/>
        <source>acknowledged-label</source>
        <translation>bestätigt</translation>
    </message>
    <message>
        <location filename="../qml/widgets/Message.qml" line="189"/>
        <source>pending-label</source>
        <translation>Bestätigung ausstehend</translation>
    </message>
</context>
<context>
    <name>MessageEditor</name>
    <message>
        <location filename="../qml/widgets/MessageEditor.qml" line="31"/>
        <source>peer-blocked-message</source>
        <extracomment>Peer is blocked</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/widgets/MessageEditor.qml" line="48"/>
        <source>peer-offline-message</source>
        <extracomment>Peer is offline, messages can&apos;t be delivered right now</extracomment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MyProfile</name>
    <message>
        <source>copy-btn</source>
        <extracomment>Button for copying profile onion address to clipboard</extracomment>
        <translation type="vanished">Kopieren</translation>
    </message>
    <message>
        <source>copied-clipboard-notification</source>
        <extracomment>Copied to clipboard</extracomment>
        <translation type="vanished">in die Zwischenablage kopiert</translation>
    </message>
    <message>
        <source>new-group-btn</source>
        <extracomment>create new group button</extracomment>
        <translation type="vanished">Neue Gruppe anlegen</translation>
    </message>
    <message>
        <source>paste-address-to-add-contact</source>
        <extracomment>ex: &quot;... paste an address here to add a contact ...&quot;</extracomment>
        <translation type="vanished">Adresse hier hinzufügen, um einen Kontakt aufzunehmen</translation>
    </message>
</context>
<context>
    <name>OverlayPane</name>
    <message>
        <source>accept-group-invite-label</source>
        <extracomment>Do you want to accept the invitation to $GROUP</extracomment>
        <translation type="vanished">Möchtest Du die Einladung annehmen</translation>
    </message>
    <message>
        <source>accept-group-btn</source>
        <extracomment>Accept group invite button</extracomment>
        <translation type="vanished">Annehmen</translation>
    </message>
    <message>
        <source>reject-group-btn</source>
        <extracomment>Reject Group invite button</extracomment>
        <translation type="vanished">Ablehnen</translation>
    </message>
    <message>
        <location filename="../qml/panes/OverlayPane.qml" line="26"/>
        <source>chat-btn</source>
        <translation>Chat</translation>
    </message>
    <message>
        <location filename="../qml/panes/OverlayPane.qml" line="26"/>
        <source>lists-btn</source>
        <translation>Listen</translation>
    </message>
    <message>
        <location filename="../qml/panes/OverlayPane.qml" line="26"/>
        <source>bulletins-btn</source>
        <translation>Meldungen</translation>
    </message>
    <message>
        <source>puzzle-game-btn</source>
        <translation type="vanished">Puzzlespiel</translation>
    </message>
</context>
<context>
    <name>PeerSettingsPane</name>
    <message>
        <location filename="../qml/panes/PeerSettingsPane.qml" line="33"/>
        <source>address-label</source>
        <translation>Adresse</translation>
    </message>
    <message>
        <location filename="../qml/panes/PeerSettingsPane.qml" line="38"/>
        <source>copy-btn</source>
        <translation>Kopieren</translation>
    </message>
    <message>
        <location filename="../qml/panes/PeerSettingsPane.qml" line="42"/>
        <source>copied-to-clipboard-notification</source>
        <extracomment>notification: copied to clipboard</extracomment>
        <translation>in die Zwischenablage kopiert</translation>
    </message>
    <message>
        <location filename="../qml/panes/PeerSettingsPane.qml" line="54"/>
        <source>display-name-label</source>
        <translation>Angezeigter Name</translation>
    </message>
    <message>
        <location filename="../qml/panes/PeerSettingsPane.qml" line="58"/>
        <source>save-btn</source>
        <translation>speichern</translation>
    </message>
    <message>
        <location filename="../qml/panes/PeerSettingsPane.qml" line="99"/>
        <location filename="../qml/panes/PeerSettingsPane.qml" line="109"/>
        <source>save-peer-history</source>
        <extracomment>Save Peer History</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/panes/PeerSettingsPane.qml" line="100"/>
        <source>save-peer-history-description</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/panes/PeerSettingsPane.qml" line="108"/>
        <source>dont-save-peer-history</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/panes/PeerSettingsPane.qml" line="130"/>
        <source>delete-btn</source>
        <translation>löschen</translation>
    </message>
    <message>
        <location filename="../qml/panes/PeerSettingsPane.qml" line="77"/>
        <source>block-btn</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ProfileAddEditPane</name>
    <message>
        <location filename="../qml/panes/ProfileAddEditPane.qml" line="179"/>
        <source>copy-btn</source>
        <translation type="unfinished">Kopieren</translation>
    </message>
    <message>
        <location filename="../qml/panes/ProfileAddEditPane.qml" line="183"/>
        <source>copied-to-clipboard-notification</source>
        <extracomment>notification: copied to clipboard</extracomment>
        <translation type="unfinished">in die Zwischenablage kopiert</translation>
    </message>
    <message>
        <location filename="../qml/panes/ProfileAddEditPane.qml" line="275"/>
        <source>radio-use-password</source>
        <extracomment>Password</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/panes/ProfileAddEditPane.qml" line="285"/>
        <source>radio-no-password</source>
        <extracomment>Unencrypted (No password)</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/panes/ProfileAddEditPane.qml" line="210"/>
        <source>no-password-warning</source>
        <extracomment>Not using a password on this account means that all data stored locally will not be encrypted</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/panes/ProfileAddEditPane.qml" line="111"/>
        <source>new-profile</source>
        <extracomment>New Profile || Edit Profile</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/panes/ProfileAddEditPane.qml" line="111"/>
        <source>edit-profile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/panes/ProfileAddEditPane.qml" line="168"/>
        <source>profile-oniblon-label</source>
        <extracomment>Send this address to peers you want to connect with</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/panes/ProfileAddEditPane.qml" line="259"/>
        <source>your-display-name</source>
        <extracomment>Your Display Name</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/panes/ProfileAddEditPane.qml" line="303"/>
        <source>current-password-label</source>
        <extracomment>Current Password</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/panes/ProfileAddEditPane.qml" line="321"/>
        <source>password1-label</source>
        <extracomment>Password</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/panes/ProfileAddEditPane.qml" line="339"/>
        <source>password2-label</source>
        <extracomment>Reenter password</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/panes/ProfileAddEditPane.qml" line="351"/>
        <source>password-error-empty</source>
        <extracomment>Passwords do not match</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/panes/ProfileAddEditPane.qml" line="374"/>
        <source>create-profile-btn</source>
        <extracomment>Create || Save</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/panes/ProfileAddEditPane.qml" line="374"/>
        <source>save-profile-btn</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/panes/ProfileAddEditPane.qml" line="351"/>
        <source>password-error-match</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/panes/ProfileAddEditPane.qml" line="362"/>
        <source>password-change-error</source>
        <extracomment>Error changing password: Supplied password rejected</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/panes/ProfileAddEditPane.qml" line="418"/>
        <source>delete-profile-btn</source>
        <extracomment>Delete Profile</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/panes/ProfileAddEditPane.qml" line="437"/>
        <location filename="../qml/panes/ProfileAddEditPane.qml" line="450"/>
        <source>delete-confirm-label</source>
        <extracomment>Type DELETE to confirm</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/panes/ProfileAddEditPane.qml" line="461"/>
        <source>delete-profile-confirm-btn</source>
        <extracomment>Really Delete Profile</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/panes/ProfileAddEditPane.qml" line="466"/>
        <source>delete-confirm-text</source>
        <extracomment>DELETE</extracomment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ProfileList</name>
    <message>
        <location filename="../qml/widgets/ProfileList.qml" line="116"/>
        <source>add-new-profile-btn</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ProfileManagerPane</name>
    <message>
        <location filename="../qml/panes/ProfileManagerPane.qml" line="36"/>
        <source>enter-profile-password</source>
        <extracomment>Enter a password to view your profiles</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/panes/ProfileManagerPane.qml" line="52"/>
        <source>password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/panes/ProfileManagerPane.qml" line="117"/>
        <source>your-profiles</source>
        <extracomment>Your Profiles</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/panes/ProfileManagerPane.qml" line="146"/>
        <source>your-servers</source>
        <extracomment>Your Profiles</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/panes/ProfileManagerPane.qml" line="63"/>
        <source>unlock</source>
        <extracomment>Unlock</extracomment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SettingsPane</name>
    <message>
        <source>cwtch-settings-title</source>
        <extracomment>Cwtch Settings title</extracomment>
        <translation type="vanished">Cwtch Einstellungen</translation>
    </message>
    <message>
        <source>zoom-label</source>
        <extracomment>Interface zoom (mostly affects text and button sizes)</extracomment>
        <translation type="vanished">Benutzeroberflächen-Zoom (betriftt hauptsächlich Text- und Knopgrößen)</translation>
    </message>
    <message>
        <location filename="../qml/panes/SettingsPane.qml" line="33"/>
        <source>setting-language</source>
        <extracomment>Language</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/panes/SettingsPane.qml" line="44"/>
        <source>locale-en</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/panes/SettingsPane.qml" line="45"/>
        <source>locale-fr</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/panes/SettingsPane.qml" line="49"/>
        <source>locale-pt</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/panes/SettingsPane.qml" line="47"/>
        <source>locale-de</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/panes/SettingsPane.qml" line="46"/>
        <source>locale-es</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/panes/SettingsPane.qml" line="48"/>
        <source>locale-it</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/panes/SettingsPane.qml" line="67"/>
        <source>setting-interface-zoom</source>
        <extracomment>Interface Zoom</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/panes/SettingsPane.qml" line="97"/>
        <source>large-text-label</source>
        <translation>Groß</translation>
    </message>
    <message>
        <location filename="../qml/panes/SettingsPane.qml" line="105"/>
        <source>setting-theme</source>
        <extracomment>Theme</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/panes/SettingsPane.qml" line="114"/>
        <source>theme-light</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/panes/SettingsPane.qml" line="115"/>
        <source>theme-dark</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/panes/SettingsPane.qml" line="134"/>
        <source>experiments-enabled</source>
        <extracomment>Theme</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/panes/SettingsPane.qml" line="164"/>
        <source>version %1 tor %2</source>
        <extracomment>Version %1 with tor %2</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/panes/SettingsPane.qml" line="171"/>
        <source>builddate %2</source>
        <extracomment>Built on: %2</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>default-scaling-text</source>
        <extracomment>&quot;Default size text (scale factor: &quot;</extracomment>
        <translation type="vanished">defaultmäßige Textgröße (Skalierungsfaktor:</translation>
    </message>
    <message>
        <location filename="../qml/panes/SettingsPane.qml" line="74"/>
        <source>small-text-label</source>
        <translation>Klein</translation>
    </message>
</context>
<context>
    <name>SplashPane</name>
    <message>
        <location filename="../qml/panes/SplashPane.qml" line="47"/>
        <source>loading-tor</source>
        <extracomment>Loading tor...</extracomment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Statusbar</name>
    <message>
        <location filename="../qml/widgets/Statusbar.qml" line="65"/>
        <source>network-status-disconnected</source>
        <extracomment>Disconnected from the internet, check your connection</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/widgets/Statusbar.qml" line="75"/>
        <source>network-status-attempting-tor</source>
        <extracomment>Attempting to connect to Tor network</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/widgets/Statusbar.qml" line="85"/>
        <source>network-status-connecting</source>
        <extracomment>Connecting...</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/widgets/Statusbar.qml" line="95"/>
        <source>network-status-online</source>
        <extracomment>Online</extracomment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <location filename="../qml/main.qml" line="265"/>
        <source>new-connection-pane-title</source>
        <extracomment>New Connection</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="398"/>
        <source>error-0-profiles-loaded-for-password</source>
        <extracomment>0 profiles loaded with that password</extracomment>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
