<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_US">
<context>
    <name>AddGroupPane</name>
    <message>
        <source>create-group-title</source>
        <translation type="vanished">Create Group</translation>
    </message>
    <message>
        <source>server-label</source>
        <extracomment>Server label</extracomment>
        <translation type="vanished">Server</translation>
    </message>
    <message>
        <source>group-name-label</source>
        <extracomment>Group name label</extracomment>
        <translation type="vanished">Group name</translation>
    </message>
    <message>
        <source>default-group-name</source>
        <extracomment>default suggested group name</extracomment>
        <translation type="vanished">Awesome Group</translation>
    </message>
    <message>
        <source>create-group-btn</source>
        <extracomment>create group button</extracomment>
        <translation type="vanished">Create</translation>
    </message>
</context>
<context>
    <name>AddPeerGroupPane</name>
    <message>
        <location filename="../qml/panes/AddPeerGroupPane.qml" line="51"/>
        <source>profile-oniblon-label</source>
        <extracomment>Send this address to peers you want to connect with</extracomment>
        <translation>Send this address to peers you want to connect with</translation>
    </message>
    <message>
        <location filename="../qml/panes/AddPeerGroupPane.qml" line="61"/>
        <source>copy-btn</source>
        <translation>Copy</translation>
    </message>
    <message>
        <location filename="../qml/panes/AddPeerGroupPane.qml" line="65"/>
        <source>copied-to-clipboard-notification</source>
        <extracomment>notification: copied to clipboard</extracomment>
        <translation>Copied to Clipboard</translation>
    </message>
    <message>
        <location filename="../qml/panes/AddPeerGroupPane.qml" line="81"/>
        <source>add-peer-tab</source>
        <translation>Add a peer</translation>
    </message>
    <message>
        <location filename="../qml/panes/AddPeerGroupPane.qml" line="81"/>
        <source>create-group-tab</source>
        <translation>Create a group</translation>
    </message>
    <message>
        <location filename="../qml/panes/AddPeerGroupPane.qml" line="81"/>
        <source>join-group-tab</source>
        <translation>Join a group</translation>
    </message>
    <message>
        <location filename="../qml/panes/AddPeerGroupPane.qml" line="107"/>
        <source>peer-address</source>
        <extracomment>Address</extracomment>
        <translation>Address</translation>
    </message>
    <message>
        <location filename="../qml/panes/AddPeerGroupPane.qml" line="119"/>
        <source>peer-name</source>
        <extracomment>Name</extracomment>
        <translation>Name</translation>
    </message>
    <message>
        <location filename="../qml/panes/AddPeerGroupPane.qml" line="140"/>
        <source>group-name</source>
        <extracomment>Group Name</extracomment>
        <translation>Group name</translation>
    </message>
    <message>
        <location filename="../qml/panes/AddPeerGroupPane.qml" line="150"/>
        <source>server</source>
        <extracomment>Server</extracomment>
        <translation>Server</translation>
    </message>
    <message>
        <source>invitation</source>
        <extracomment>Invitation</extracomment>
        <translation type="vanished">Invitation</translation>
    </message>
    <message>
        <location filename="../qml/panes/AddPeerGroupPane.qml" line="177"/>
        <source>group-addr</source>
        <extracomment>Address</extracomment>
        <translation>Address</translation>
    </message>
    <message>
        <source>add-peer</source>
        <extracomment>Add Peer | Create Group | Join Group</extracomment>
        <translation type="vanished">Add Peer</translation>
    </message>
    <message>
        <source>create-group</source>
        <translation type="vanished">Create group</translation>
    </message>
    <message>
        <source>join-group</source>
        <translation type="vanished">Join group</translation>
    </message>
</context>
<context>
    <name>BulletinOverlay</name>
    <message>
        <location filename="../qml/overlays/BulletinOverlay.qml" line="215"/>
        <source>new-bulletin-label</source>
        <translation>New Bulletin</translation>
    </message>
    <message>
        <location filename="../qml/overlays/BulletinOverlay.qml" line="227"/>
        <source>post-new-bulletin-label</source>
        <extracomment>Post a new Bulletin Post</extracomment>
        <translation>Post new bulletin</translation>
    </message>
    <message>
        <location filename="../qml/overlays/BulletinOverlay.qml" line="234"/>
        <source>title-placeholder</source>
        <extracomment>title place holder text</extracomment>
        <translation>title...</translation>
    </message>
</context>
<context>
    <name>ChatOverlay</name>
    <message>
        <location filename="../qml/overlays/ChatOverlay.qml" line="69"/>
        <source>chat-history-default</source>
        <extracomment>This conversation will be deleted when Cwtch is closed! Message history can be enabled per-conversation via the Settings menu in the upper right.</extracomment>
        <translation>Your history with this peer is ephemeral and will not be saved. If you would like to save history, please go to settings and turn it on.</translation>
    </message>
    <message>
        <location filename="../qml/overlays/ChatOverlay.qml" line="71"/>
        <source>chat-history-disabled</source>
        <extracomment>Message history is disabled.</extracomment>
        <translation>Message history is disabled.</translation>
    </message>
    <message>
        <location filename="../qml/overlays/ChatOverlay.qml" line="73"/>
        <source>chat-history-enabled</source>
        <extracomment>Message history is enabled.</extracomment>
        <translation>Message history is enabled.</translation>
    </message>
</context>
<context>
    <name>ContactList</name>
    <message>
        <source>paste-address-to-add-contact</source>
        <extracomment>ex: &quot;... paste an address here to add a contact ...&quot;</extracomment>
        <translation type="vanished">... paste an address here to add a contact...</translation>
    </message>
    <message>
        <location filename="../qml/widgets/ContactList.qml" line="252"/>
        <source>blocked</source>
        <translation>Blocked</translation>
    </message>
</context>
<context>
    <name>EmojiDrawer</name>
    <message>
        <source>cycle-cats-android</source>
        <translation type="vanished">Click to cycle category.
Long-press to reset.</translation>
    </message>
    <message>
        <source>cycle-cats-desktop</source>
        <translation type="vanished">Click to cycle category.
Right-click to reset.</translation>
    </message>
    <message>
        <source>cycle-morphs-android</source>
        <translation type="vanished">Click to cycle morphs.
Long-press to reset.</translation>
    </message>
    <message>
        <source>cycle-morphs-desktop</source>
        <translation type="vanished">Click to cycle morphs.
Right-click to reset.</translation>
    </message>
    <message>
        <source>cycle-colours-android</source>
        <translation type="vanished">Click to cycle colours.
Long-press to reset.</translation>
    </message>
    <message>
        <source>cycle-colours-desktop</source>
        <translation type="vanished">Click to cycle colours.
Right-click to reset.</translation>
    </message>
    <message>
        <source>search</source>
        <extracomment>Search...</extracomment>
        <translation type="vanished">Search...</translation>
    </message>
    <message>
        <source>emojicat-expressions</source>
        <extracomment>Expressions</extracomment>
        <translation type="vanished">Expressions</translation>
    </message>
    <message>
        <source>emojicat-activities</source>
        <extracomment>Activities</extracomment>
        <translation type="vanished">Activities</translation>
    </message>
    <message>
        <source>emojicat-food</source>
        <extracomment>Food, drink &amp; herbs</extracomment>
        <translation type="vanished">Food, drink &amp; herbs</translation>
    </message>
    <message>
        <source>emojicat-gender</source>
        <extracomment>Gender, relationships &amp; sexuality</extracomment>
        <translation type="vanished">Gender, relationships &amp; sexuality</translation>
    </message>
    <message>
        <source>emojicat-nature</source>
        <extracomment>Nature and effects</extracomment>
        <translation type="vanished">Nature and effects</translation>
    </message>
    <message>
        <source>emojicat-objects</source>
        <extracomment>Objects</extracomment>
        <translation type="vanished">Objects</translation>
    </message>
    <message>
        <source>emojicat-people</source>
        <extracomment>People and animals</extracomment>
        <translation type="vanished">People and animals</translation>
    </message>
    <message>
        <source>emojicat-symbols</source>
        <extracomment>Symbols</extracomment>
        <translation type="vanished">Symbols</translation>
    </message>
    <message>
        <source>emojicat-travel</source>
        <extracomment>Travel &amp; places</extracomment>
        <translation type="vanished">Travel &amp; places</translation>
    </message>
    <message>
        <source>emojicat-misc</source>
        <extracomment>Miscellaneous</extracomment>
        <translation type="vanished">Miscellaneous</translation>
    </message>
</context>
<context>
    <name>GroupSettingsPane</name>
    <message>
        <location filename="../qml/panes/GroupSettingsPane.qml" line="45"/>
        <source>server-label</source>
        <translation>Server</translation>
    </message>
    <message>
        <location filename="../qml/panes/GroupSettingsPane.qml" line="50"/>
        <location filename="../qml/panes/GroupSettingsPane.qml" line="68"/>
        <source>copy-btn</source>
        <translation>Copy</translation>
    </message>
    <message>
        <location filename="../qml/panes/GroupSettingsPane.qml" line="54"/>
        <location filename="../qml/panes/GroupSettingsPane.qml" line="72"/>
        <source>copied-to-clipboard-notification</source>
        <extracomment>notification: copied to clipboard</extracomment>
        <translation>Copied to Clipboard</translation>
    </message>
    <message>
        <location filename="../qml/panes/GroupSettingsPane.qml" line="63"/>
        <source>invitation-label</source>
        <translation>Invitation</translation>
    </message>
    <message>
        <location filename="../qml/panes/GroupSettingsPane.qml" line="82"/>
        <source>server-info</source>
        <translation>Server Information</translation>
    </message>
    <message>
        <location filename="../qml/panes/GroupSettingsPane.qml" line="90"/>
        <source>server-connectivity-connected</source>
        <translation>Server Connected</translation>
    </message>
    <message>
        <location filename="../qml/panes/GroupSettingsPane.qml" line="90"/>
        <source>server-connectivity-disconnected</source>
        <translation>Server Disconnected</translation>
    </message>
    <message>
        <location filename="../qml/panes/GroupSettingsPane.qml" line="108"/>
        <source>server-synced</source>
        <translation>Synced</translation>
    </message>
    <message>
        <location filename="../qml/panes/GroupSettingsPane.qml" line="108"/>
        <source>server-not-synced</source>
        <translation>Out of Sync</translation>
    </message>
    <message>
        <location filename="../qml/panes/GroupSettingsPane.qml" line="124"/>
        <source>view-server-info</source>
        <translation>Server Info</translation>
    </message>
    <message>
        <location filename="../qml/panes/GroupSettingsPane.qml" line="28"/>
        <source>group-name-label</source>
        <translation>Group Name</translation>
    </message>
    <message>
        <location filename="../qml/panes/GroupSettingsPane.qml" line="33"/>
        <source>save-btn</source>
        <translation>Save</translation>
    </message>
    <message>
        <source>invite-to-group-label</source>
        <extracomment>Invite someone to the group</extracomment>
        <translation type="vanished">Invite to group</translation>
    </message>
    <message>
        <source>invite-btn</source>
        <translation type="vanished">Invite</translation>
    </message>
    <message>
        <location filename="../qml/panes/GroupSettingsPane.qml" line="144"/>
        <source>delete-btn</source>
        <translation>Delete</translation>
    </message>
</context>
<context>
    <name>InplaceEditText</name>
    <message>
        <source>Update</source>
        <translation type="vanished">Update</translation>
    </message>
</context>
<context>
    <name>ListOverlay</name>
    <message>
        <location filename="../qml/overlays/ListOverlay.qml" line="33"/>
        <source>search-list</source>
        <extracomment>ex: &quot;Find...&quot;</extracomment>
        <translation>Search List</translation>
    </message>
    <message>
        <location filename="../qml/overlays/ListOverlay.qml" line="63"/>
        <source>peer-not-online</source>
        <translation>Peer is Offline. Applications cannot be used right now.</translation>
    </message>
    <message>
        <location filename="../qml/overlays/ListOverlay.qml" line="212"/>
        <source>add-list-item-btn</source>
        <translation>Add Item</translation>
    </message>
</context>
<context>
    <name>MembershipOverlay</name>
    <message>
        <location filename="../qml/overlays/MembershipOverlay.qml" line="21"/>
        <source>membership-description</source>
        <extracomment>Below is a list of users who have sent messages to the group. This list may not reflect all users who have access to the group.</extracomment>
        <translation>Below is a list of users who have sent messages to the group. This list may not reflect all users who have access to the group.</translation>
    </message>
</context>
<context>
    <name>Message</name>
    <message>
        <location filename="../qml/widgets/Message.qml" line="67"/>
        <source>dm-tooltip</source>
        <extracomment>Click to DM</extracomment>
        <translation>Click to DM</translation>
    </message>
    <message>
        <location filename="../qml/widgets/Message.qml" line="189"/>
        <source>could-not-send-msg-error</source>
        <extracomment>Could not send this message</extracomment>
        <translation>Could not send this message</translation>
    </message>
    <message>
        <location filename="../qml/widgets/Message.qml" line="189"/>
        <source>acknowledged-label</source>
        <translation>Acknowledged</translation>
    </message>
    <message>
        <location filename="../qml/widgets/Message.qml" line="189"/>
        <source>pending-label</source>
        <translation>Pending</translation>
    </message>
</context>
<context>
    <name>MessageEditor</name>
    <message>
        <location filename="../qml/widgets/MessageEditor.qml" line="31"/>
        <source>peer-blocked-message</source>
        <extracomment>Peer is blocked</extracomment>
        <translation>Peer is blocked</translation>
    </message>
    <message>
        <location filename="../qml/widgets/MessageEditor.qml" line="48"/>
        <source>peer-offline-message</source>
        <extracomment>Peer is offline, messages can&apos;t be delivered right now</extracomment>
        <translation>Peer is offline, messages can&apos;t be delivered right now</translation>
    </message>
</context>
<context>
    <name>MyProfile</name>
    <message>
        <source>copy-btn</source>
        <extracomment>Button for copying profile onion address to clipboard</extracomment>
        <translation type="vanished">Copy</translation>
    </message>
    <message>
        <source>copied-clipboard-notification</source>
        <extracomment>Copied to clipboard</extracomment>
        <translation type="vanished">Copied to clipboard</translation>
    </message>
    <message>
        <source>new-group-btn</source>
        <extracomment>create new group button</extracomment>
        <translation type="vanished">Create new group</translation>
    </message>
    <message>
        <source>paste-address-to-add-contact</source>
        <extracomment>ex: &quot;... paste an address here to add a contact ...&quot;</extracomment>
        <translation type="vanished">... paste an address here to add a contact...</translation>
    </message>
</context>
<context>
    <name>OverlayPane</name>
    <message>
        <source>accept-group-invite-label</source>
        <extracomment>Do you want to accept the invitation to $GROUP</extracomment>
        <translation type="vanished">Do you want to accept the invitation to</translation>
    </message>
    <message>
        <source>accept-group-btn</source>
        <extracomment>Accept group invite button</extracomment>
        <translation type="vanished">Accept</translation>
    </message>
    <message>
        <source>reject-group-btn</source>
        <extracomment>Reject Group invite button</extracomment>
        <translation type="vanished">Reject</translation>
    </message>
    <message>
        <location filename="../qml/panes/OverlayPane.qml" line="26"/>
        <source>chat-btn</source>
        <translation>Chat</translation>
    </message>
    <message>
        <location filename="../qml/panes/OverlayPane.qml" line="26"/>
        <source>lists-btn</source>
        <translation>Lists</translation>
    </message>
    <message>
        <location filename="../qml/panes/OverlayPane.qml" line="26"/>
        <source>bulletins-btn</source>
        <translation>Bulletins</translation>
    </message>
    <message>
        <source>puzzle-game-btn</source>
        <translation type="vanished">Puzzle Game</translation>
    </message>
</context>
<context>
    <name>PeerSettingsPane</name>
    <message>
        <location filename="../qml/panes/PeerSettingsPane.qml" line="33"/>
        <source>address-label</source>
        <translation>Address</translation>
    </message>
    <message>
        <location filename="../qml/panes/PeerSettingsPane.qml" line="38"/>
        <source>copy-btn</source>
        <translation>Copy</translation>
    </message>
    <message>
        <location filename="../qml/panes/PeerSettingsPane.qml" line="42"/>
        <source>copied-to-clipboard-notification</source>
        <extracomment>notification: copied to clipboard</extracomment>
        <translation>Copied to Clipboard</translation>
    </message>
    <message>
        <location filename="../qml/panes/PeerSettingsPane.qml" line="54"/>
        <source>display-name-label</source>
        <translation>Display Name</translation>
    </message>
    <message>
        <location filename="../qml/panes/PeerSettingsPane.qml" line="58"/>
        <source>save-btn</source>
        <translation>Save</translation>
    </message>
    <message>
        <location filename="../qml/panes/PeerSettingsPane.qml" line="77"/>
        <source>block-btn</source>
        <translation>Block Peer</translation>
    </message>
    <message>
        <location filename="../qml/panes/PeerSettingsPane.qml" line="99"/>
        <location filename="../qml/panes/PeerSettingsPane.qml" line="109"/>
        <source>save-peer-history</source>
        <extracomment>Save Peer History</extracomment>
        <translation>Save Peer History</translation>
    </message>
    <message>
        <location filename="../qml/panes/PeerSettingsPane.qml" line="100"/>
        <source>save-peer-history-description</source>
        <translation>Determines whether or not to delete any history associated with the peer.</translation>
    </message>
    <message>
        <location filename="../qml/panes/PeerSettingsPane.qml" line="108"/>
        <source>dont-save-peer-history</source>
        <translation>Delete Peer History</translation>
    </message>
    <message>
        <source>unblock-btn</source>
        <translation type="vanished">Unblock Peer</translation>
    </message>
    <message>
        <location filename="../qml/panes/PeerSettingsPane.qml" line="130"/>
        <source>delete-btn</source>
        <translation>Delete</translation>
    </message>
</context>
<context>
    <name>ProfileAddEditPane</name>
    <message>
        <source>add-profile-title</source>
        <translation type="vanished">Add new profile</translation>
    </message>
    <message>
        <source>edit-profile-title</source>
        <translation type="vanished">Edit Profile</translation>
    </message>
    <message>
        <source>profile-name</source>
        <extracomment>Display name</extracomment>
        <translation type="vanished">Display name</translation>
    </message>
    <message>
        <source>default-profile-name</source>
        <translation type="vanished">Alice</translation>
    </message>
    <message>
        <location filename="../qml/panes/ProfileAddEditPane.qml" line="111"/>
        <source>new-profile</source>
        <extracomment>New Profile || Edit Profile</extracomment>
        <translation>New Profile</translation>
    </message>
    <message>
        <location filename="../qml/panes/ProfileAddEditPane.qml" line="111"/>
        <source>edit-profile</source>
        <translation>Edit Profille</translation>
    </message>
    <message>
        <source>profile-onion-label</source>
        <extracomment>Send this address to peers you want to connect with</extracomment>
        <translation type="vanished">Send this address to peers you want to connect with</translation>
    </message>
    <message>
        <location filename="../qml/panes/ProfileAddEditPane.qml" line="179"/>
        <source>copy-btn</source>
        <translation>Copy</translation>
    </message>
    <message>
        <location filename="../qml/panes/ProfileAddEditPane.qml" line="183"/>
        <source>copied-to-clipboard-notification</source>
        <extracomment>notification: copied to clipboard</extracomment>
        <translation>Copied to Clipboard</translation>
    </message>
    <message>
        <location filename="../qml/panes/ProfileAddEditPane.qml" line="275"/>
        <source>radio-use-password</source>
        <extracomment>Password</extracomment>
        <translation>Password</translation>
    </message>
    <message>
        <location filename="../qml/panes/ProfileAddEditPane.qml" line="285"/>
        <source>radio-no-password</source>
        <extracomment>Unencrypted (No password)</extracomment>
        <translation>Unencrypted (No password)</translation>
    </message>
    <message>
        <location filename="../qml/panes/ProfileAddEditPane.qml" line="210"/>
        <source>no-password-warning</source>
        <extracomment>Not using a password on this account means that all data stored locally will not be encrypted</extracomment>
        <translation>Not using a password on this account means that all data stored locally will not be encrypted</translation>
    </message>
    <message>
        <location filename="../qml/panes/ProfileAddEditPane.qml" line="168"/>
        <source>profile-oniblon-label</source>
        <extracomment>Send this address to peers you want to connect with</extracomment>
        <translation>Send this address to peers you want to connect with</translation>
    </message>
    <message>
        <location filename="../qml/panes/ProfileAddEditPane.qml" line="259"/>
        <source>your-display-name</source>
        <extracomment>Your Display Name</extracomment>
        <translation>Your Display Name</translation>
    </message>
    <message>
        <location filename="../qml/panes/ProfileAddEditPane.qml" line="303"/>
        <source>current-password-label</source>
        <extracomment>Current Password</extracomment>
        <translation>Current Password</translation>
    </message>
    <message>
        <location filename="../qml/panes/ProfileAddEditPane.qml" line="321"/>
        <source>password1-label</source>
        <extracomment>Password</extracomment>
        <translation>Password</translation>
    </message>
    <message>
        <location filename="../qml/panes/ProfileAddEditPane.qml" line="339"/>
        <source>password2-label</source>
        <extracomment>Reenter password</extracomment>
        <translation>Reenter password</translation>
    </message>
    <message>
        <location filename="../qml/panes/ProfileAddEditPane.qml" line="351"/>
        <source>password-error-empty</source>
        <extracomment>Passwords do not match</extracomment>
        <translation>Password cannot be empty</translation>
    </message>
    <message>
        <location filename="../qml/panes/ProfileAddEditPane.qml" line="374"/>
        <source>create-profile-btn</source>
        <extracomment>Create || Save</extracomment>
        <translation>Create Profile</translation>
    </message>
    <message>
        <location filename="../qml/panes/ProfileAddEditPane.qml" line="374"/>
        <source>save-profile-btn</source>
        <translation>Save Profile</translation>
    </message>
    <message>
        <location filename="../qml/panes/ProfileAddEditPane.qml" line="351"/>
        <source>password-error-match</source>
        <translation>Passwords do not match</translation>
    </message>
    <message>
        <location filename="../qml/panes/ProfileAddEditPane.qml" line="362"/>
        <source>password-change-error</source>
        <extracomment>Error changing password: Supplied password rejected</extracomment>
        <translation>Error changing password: Supplied password rejected</translation>
    </message>
    <message>
        <location filename="../qml/panes/ProfileAddEditPane.qml" line="418"/>
        <source>delete-profile-btn</source>
        <extracomment>Delete Profile</extracomment>
        <translation>Delete Profile</translation>
    </message>
    <message>
        <location filename="../qml/panes/ProfileAddEditPane.qml" line="437"/>
        <location filename="../qml/panes/ProfileAddEditPane.qml" line="450"/>
        <source>delete-confirm-label</source>
        <extracomment>Type DELETE to confirm</extracomment>
        <translation>Type DELETE to confirm</translation>
    </message>
    <message>
        <location filename="../qml/panes/ProfileAddEditPane.qml" line="461"/>
        <source>delete-profile-confirm-btn</source>
        <extracomment>Really Delete Profile</extracomment>
        <translation>Really Delete Profile</translation>
    </message>
    <message>
        <location filename="../qml/panes/ProfileAddEditPane.qml" line="466"/>
        <source>delete-confirm-text</source>
        <extracomment>DELETE</extracomment>
        <translation>DELETE</translation>
    </message>
</context>
<context>
    <name>ProfileList</name>
    <message>
        <location filename="../qml/widgets/ProfileList.qml" line="116"/>
        <source>add-new-profile-btn</source>
        <translation>Add new profile</translation>
    </message>
</context>
<context>
    <name>ProfileManagerPane</name>
    <message>
        <location filename="../qml/panes/ProfileManagerPane.qml" line="36"/>
        <source>enter-profile-password</source>
        <extracomment>Enter a password to view your profiles</extracomment>
        <translation>Enter a password to view your profiles</translation>
    </message>
    <message>
        <location filename="../qml/panes/ProfileManagerPane.qml" line="52"/>
        <source>password</source>
        <translation>Password</translation>
    </message>
    <message>
        <source>error-0-profiles-loaded-for-password</source>
        <extracomment>0 profiles loaded with that password</extracomment>
        <translation type="vanished">0 profiles loaded with that password</translation>
    </message>
    <message>
        <location filename="../qml/panes/ProfileManagerPane.qml" line="117"/>
        <source>your-profiles</source>
        <extracomment>Your Profiles</extracomment>
        <translation>Your Profiles</translation>
    </message>
    <message>
        <location filename="../qml/panes/ProfileManagerPane.qml" line="146"/>
        <source>your-servers</source>
        <extracomment>Your Profiles</extracomment>
        <translation>Your Servers</translation>
    </message>
    <message>
        <location filename="../qml/panes/ProfileManagerPane.qml" line="63"/>
        <source>unlock</source>
        <extracomment>Unlock</extracomment>
        <translation>Unlock</translation>
    </message>
</context>
<context>
    <name>SettingsPane</name>
    <message>
        <source>cwtch-settings-title</source>
        <extracomment>Cwtch Settings title</extracomment>
        <translation type="vanished">Cwtch Settings</translation>
    </message>
    <message>
        <source>version %1 builddate %2</source>
        <extracomment>Version: %1 Built on: %2</extracomment>
        <translation type="vanished">Version: %1 Built on: %2</translation>
    </message>
    <message>
        <source>zoom-label</source>
        <extracomment>Interface zoom (mostly affects text and button sizes)</extracomment>
        <translation type="vanished">Interface zoom (mostly affects text and button sizes)</translation>
    </message>
    <message>
        <source>block-unknown-label</source>
        <translation type="vanished">Block Unknown Peers</translation>
    </message>
    <message>
        <location filename="../qml/panes/SettingsPane.qml" line="33"/>
        <source>setting-language</source>
        <extracomment>Language</extracomment>
        <translation>Language</translation>
    </message>
    <message>
        <location filename="../qml/panes/SettingsPane.qml" line="44"/>
        <source>locale-en</source>
        <translation>English</translation>
    </message>
    <message>
        <location filename="../qml/panes/SettingsPane.qml" line="45"/>
        <source>locale-fr</source>
        <translation>Frances</translation>
    </message>
    <message>
        <location filename="../qml/panes/SettingsPane.qml" line="49"/>
        <source>locale-pt</source>
        <translation>Portuguesa</translation>
    </message>
    <message>
        <location filename="../qml/panes/SettingsPane.qml" line="47"/>
        <source>locale-de</source>
        <translation>Deutsche</translation>
    </message>
    <message>
        <location filename="../qml/panes/SettingsPane.qml" line="46"/>
        <source>locale-es</source>
        <translation>Espanol</translation>
    </message>
    <message>
        <location filename="../qml/panes/SettingsPane.qml" line="48"/>
        <source>locale-it</source>
        <translation>Italiana</translation>
    </message>
    <message>
        <location filename="../qml/panes/SettingsPane.qml" line="67"/>
        <source>setting-interface-zoom</source>
        <extracomment>Interface Zoom</extracomment>
        <translation>Zoom level</translation>
    </message>
    <message>
        <location filename="../qml/panes/SettingsPane.qml" line="97"/>
        <source>large-text-label</source>
        <translation>Large</translation>
    </message>
    <message>
        <location filename="../qml/panes/SettingsPane.qml" line="105"/>
        <source>setting-theme</source>
        <extracomment>Theme</extracomment>
        <translation>Theme</translation>
    </message>
    <message>
        <location filename="../qml/panes/SettingsPane.qml" line="114"/>
        <source>theme-light</source>
        <translation>Light</translation>
    </message>
    <message>
        <location filename="../qml/panes/SettingsPane.qml" line="115"/>
        <source>theme-dark</source>
        <translation>Dark</translation>
    </message>
    <message>
        <location filename="../qml/panes/SettingsPane.qml" line="134"/>
        <source>experiments-enabled</source>
        <extracomment>Theme</extracomment>
        <translation>Experiments enabled</translation>
    </message>
    <message>
        <location filename="../qml/panes/SettingsPane.qml" line="164"/>
        <source>version %1 tor %2</source>
        <extracomment>Version %1 with tor %2</extracomment>
        <translation>Version %1 with tor %2</translation>
    </message>
    <message>
        <source>version %1</source>
        <extracomment>Version %1</extracomment>
        <translation type="vanished">Version %1</translation>
    </message>
    <message>
        <location filename="../qml/panes/SettingsPane.qml" line="171"/>
        <source>builddate %2</source>
        <extracomment>Built on: %2</extracomment>
        <translation>Built on: %2</translation>
    </message>
    <message>
        <source>default-scaling-text</source>
        <extracomment>&quot;Default size text (scale factor: &quot;</extracomment>
        <translation type="vanished">Default size text (scale factor:</translation>
    </message>
    <message>
        <location filename="../qml/panes/SettingsPane.qml" line="74"/>
        <source>small-text-label</source>
        <translation>Small</translation>
    </message>
</context>
<context>
    <name>SplashPane</name>
    <message>
        <location filename="../qml/panes/SplashPane.qml" line="47"/>
        <source>loading-tor</source>
        <extracomment>Loading tor...</extracomment>
        <translation>Loading tor...</translation>
    </message>
</context>
<context>
    <name>StackToolbar</name>
    <message>
        <source>view-group-membership-tooltip</source>
        <extracomment>View Group Membership</extracomment>
        <translation type="vanished">View Group Membership</translation>
    </message>
</context>
<context>
    <name>Statusbar</name>
    <message>
        <location filename="../qml/widgets/Statusbar.qml" line="65"/>
        <source>network-status-disconnected</source>
        <extracomment>Disconnected from the internet, check your connection</extracomment>
        <translation>Disconnected from the internet, check your connection</translation>
    </message>
    <message>
        <location filename="../qml/widgets/Statusbar.qml" line="75"/>
        <source>network-status-attempting-tor</source>
        <extracomment>Attempting to connect to Tor network</extracomment>
        <translation>Attempting to connect to Tor network</translation>
    </message>
    <message>
        <location filename="../qml/widgets/Statusbar.qml" line="85"/>
        <source>network-status-connecting</source>
        <extracomment>Connecting...</extracomment>
        <translation>Connecting to network and peers...</translation>
    </message>
    <message>
        <location filename="../qml/widgets/Statusbar.qml" line="95"/>
        <source>network-status-online</source>
        <extracomment>Online</extracomment>
        <translation>Online</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <location filename="../qml/main.qml" line="265"/>
        <source>new-connection-pane-title</source>
        <extracomment>New Connection</extracomment>
        <translation>New Connection</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="398"/>
        <source>error-0-profiles-loaded-for-password</source>
        <extracomment>0 profiles loaded with that password</extracomment>
        <translation>0 profiles loaded with that password</translation>
    </message>
</context>
</TS>
