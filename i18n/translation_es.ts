<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="es">
<context>
    <name>AddGroupPane</name>
    <message>
        <source>create-group-title</source>
        <translation type="vanished">Crear un grupo</translation>
    </message>
    <message>
        <source>server-label</source>
        <translation type="vanished">Servidor</translation>
    </message>
    <message>
        <source>group-name-label</source>
        <translation type="vanished">Nombre del grupo</translation>
    </message>
    <message>
        <source>default-group-name</source>
        <translation type="vanished">El Grupo Asombroso</translation>
    </message>
    <message>
        <source>create-group-btn</source>
        <translation type="vanished">Crear</translation>
    </message>
</context>
<context>
    <name>AddPeerGroupPane</name>
    <message>
        <source>profile-oniblon-label</source>
        <extracomment>Send this address to peers you want to connect with</extracomment>
        <translation>Envía esta dirección a los contactos con los que quieras conectarte</translation>
    </message>
    <message>
        <source>copy-btn</source>
        <translation>Copiar</translation>
    </message>
    <message>
        <source>copied-to-clipboard-notification</source>
        <extracomment>notification: copied to clipboard</extracomment>
        <translation>Copiado al portapapeles</translation>
    </message>
    <message>
        <source>add-peer-tab</source>
        <translation>Agregar Contacto</translation>
    </message>
    <message>
        <source>create-group-tab</source>
        <translation>Crear un grupo</translation>
    </message>
    <message>
        <source>join-group-tab</source>
        <translation>Únete a un grupo</translation>
    </message>
    <message>
        <source>peer-address</source>
        <extracomment>Address</extracomment>
        <translation>Dirección</translation>
    </message>
    <message>
        <source>peer-name</source>
        <extracomment>Name</extracomment>
        <translation>Nombre</translation>
    </message>
    <message>
        <source>group-name</source>
        <extracomment>Group Name</extracomment>
        <translation>Nombre del grupo</translation>
    </message>
    <message>
        <source>server</source>
        <extracomment>Server</extracomment>
        <translation>Servidor</translation>
    </message>
    <message>
        <source>invitation</source>
        <extracomment>Invitation</extracomment>
        <translation type="vanished">Invitación</translation>
    </message>
    <message>
        <source>group-addr</source>
        <extracomment>Address</extracomment>
        <translation>Dirección</translation>
    </message>
    <message>
        <source>add-peer</source>
        <translation type="vanished">Agregar Contacto</translation>
    </message>
    <message>
        <source>create-group</source>
        <translation type="vanished">Crear perfil</translation>
    </message>
    <message>
        <source>join-group</source>
        <translation type="vanished">Únete al grupo</translation>
    </message>
</context>
<context>
    <name>BulletinOverlay</name>
    <message>
        <source>new-bulletin-label</source>
        <translation>Nuevo Boletín</translation>
    </message>
    <message>
        <source>post-new-bulletin-label</source>
        <extracomment>Post a new Bulletin Post</extracomment>
        <translation>Publicar nuevo boletín</translation>
    </message>
    <message>
        <source>title-placeholder</source>
        <extracomment>title place holder text</extracomment>
        <translation>título...</translation>
    </message>
</context>
<context>
    <name>ChatOverlay</name>
    <message>
        <source>chat-history-default</source>
        <extracomment>This conversation will be deleted when Cwtch is closed! Message history can be enabled per-conversation via the Settings menu in the upper right.</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>chat-history-disabled</source>
        <extracomment>Message history is disabled.</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>chat-history-enabled</source>
        <extracomment>Message history is enabled.</extracomment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ContactList</name>
    <message>
        <source>paste-address-to-add-contact</source>
        <translation type="vanished">...pegar una dirección aquí para añadir contacto...</translation>
    </message>
    <message>
        <source>blocked</source>
        <translation>Bloqueado</translation>
    </message>
</context>
<context>
    <name>EmojiDrawer</name>
    <message>
        <source>cycle-cats-android</source>
        <translation type="vanished">Click para cambiar categoría. Mantenga pulsado para reiniciar.</translation>
    </message>
    <message>
        <source>cycle-cats-desktop</source>
        <translation type="vanished">Click para cambiar categoría. Click derecho para reiniciar.</translation>
    </message>
    <message>
        <source>cycle-morphs-android</source>
        <translation type="vanished">Click para cambiar transformaciones. Mantenga pulsado para reiniciar.</translation>
    </message>
    <message>
        <source>cycle-morphs-desktop</source>
        <translation type="vanished">Click para cambiar transformaciones. Click derecho para reiniciar.</translation>
    </message>
    <message>
        <source>cycle-colours-android</source>
        <translation type="vanished">Click para cambiar colores. Mantenga pulsado para reiniciar.</translation>
    </message>
    <message>
        <source>cycle-colours-desktop</source>
        <translation type="vanished">Click para cambiar colores. Click derecho para reiniciar.</translation>
    </message>
    <message>
        <source>search</source>
        <translation type="vanished">Búsqueda...</translation>
    </message>
</context>
<context>
    <name>GroupSettingsPane</name>
    <message>
        <source>server-label</source>
        <translation>Servidor</translation>
    </message>
    <message>
        <source>copy-btn</source>
        <translation>Copiar</translation>
    </message>
    <message>
        <source>copied-to-clipboard-notification</source>
        <extracomment>notification: copied to clipboard</extracomment>
        <translation>Copiado al portapapeles</translation>
    </message>
    <message>
        <source>invitation-label</source>
        <translation>Invitación</translation>
    </message>
    <message>
        <source>server-info</source>
        <translation>Información del servidor</translation>
    </message>
    <message>
        <source>server-connectivity-connected</source>
        <translation>Servidor conectado</translation>
    </message>
    <message>
        <source>server-connectivity-disconnected</source>
        <translation>Servidor desconectado</translation>
    </message>
    <message>
        <source>server-synced</source>
        <translation>Sincronizado</translation>
    </message>
    <message>
        <source>server-not-synced</source>
        <translation>Fuera de sincronización con el servidor</translation>
    </message>
    <message>
        <source>view-server-info</source>
        <translation>Información del servidor</translation>
    </message>
    <message>
        <source>group-name-label</source>
        <translation>Nombre del grupo</translation>
    </message>
    <message>
        <source>save-btn</source>
        <translation>Guardar</translation>
    </message>
    <message>
        <source>invite-to-group-label</source>
        <translation type="vanished">Invitar al grupo</translation>
    </message>
    <message>
        <source>invite-btn</source>
        <translation type="vanished">Invitar</translation>
    </message>
    <message>
        <source>delete-btn</source>
        <translation>Eliminar</translation>
    </message>
</context>
<context>
    <name>InplaceEditText</name>
    <message>
        <source>Update</source>
        <translation type="vanished">Actualizar</translation>
    </message>
</context>
<context>
    <name>ListOverlay</name>
    <message>
        <source>search-list</source>
        <extracomment>ex: &quot;Find...&quot;</extracomment>
        <translation>Buscar en la lista</translation>
    </message>
    <message>
        <source>peer-not-online</source>
        <translation>Este contacto no está en línea, la aplicación no puede ser usada en este momento</translation>
    </message>
    <message>
        <source>add-list-item-btn</source>
        <translation>Agregar artículo</translation>
    </message>
    <message>
        <source>add-list-item</source>
        <translation type="vanished">Añadir un nuevo elemento a la lista</translation>
    </message>
    <message>
        <source>add-new-item</source>
        <translation type="vanished">Añadir un nuevo elemento a la lista</translation>
    </message>
    <message>
        <source>todo-placeholder</source>
        <translation type="vanished">Por hacer...</translation>
    </message>
</context>
<context>
    <name>MembershipOverlay</name>
    <message>
        <source>membership-description</source>
        <extracomment>Below is a list of users who have sent messages to the group. This list may not reflect all users who have access to the group.</extracomment>
        <translation>La lista a continuación solo muestra los miembros que han enviado mensajes al grupo, no incluye a todos los usuarios dentro del grupo</translation>
    </message>
</context>
<context>
    <name>Message</name>
    <message>
        <source>dm-tooltip</source>
        <extracomment>Click to DM</extracomment>
        <translation>Haz clic para enviar mensaje directo</translation>
    </message>
    <message>
        <source>could-not-send-msg-error</source>
        <extracomment>Could not send this message</extracomment>
        <translation>No se pudo enviar este mensaje</translation>
    </message>
    <message>
        <source>acknowledged-label</source>
        <translation>Reconocido</translation>
    </message>
    <message>
        <source>pending-label</source>
        <translation>Pendiente</translation>
    </message>
</context>
<context>
    <name>MessageEditor</name>
    <message>
        <source>peer-blocked-message</source>
        <extracomment>Peer is blocked</extracomment>
        <translation>Contacto bloqueado</translation>
    </message>
    <message>
        <source>peer-offline-message</source>
        <extracomment>Peer is offline, messages can&apos;t be delivered right now</extracomment>
        <translation>Este contacto no está en línea, los mensajes no pueden ser entregados en este momento</translation>
    </message>
</context>
<context>
    <name>MyProfile</name>
    <message>
        <source>copy-btn</source>
        <translation type="vanished">Copiar</translation>
    </message>
    <message>
        <source>copied-clipboard-notification</source>
        <translation type="vanished">Copiado al portapapeles</translation>
    </message>
    <message>
        <source>new-group-btn</source>
        <translation type="vanished">Crear un nuevo grupo de chat</translation>
    </message>
    <message>
        <source>paste-address-to-add-contact</source>
        <translation type="vanished">...pegar una dirección aquí para añadir un contacto...</translation>
    </message>
</context>
<context>
    <name>OverlayPane</name>
    <message>
        <source>accept-group-invite-label</source>
        <translation type="vanished">¿Quieres aceptar la invitación a </translation>
    </message>
    <message>
        <source>accept-group-btn</source>
        <translation type="vanished">Aceptar</translation>
    </message>
    <message>
        <source>reject-group-btn</source>
        <translation type="vanished">Rechazar</translation>
    </message>
    <message>
        <source>chat-btn</source>
        <translation>Chat</translation>
    </message>
    <message>
        <source>lists-btn</source>
        <translation>Listas</translation>
    </message>
    <message>
        <source>bulletins-btn</source>
        <translation>Boletines</translation>
    </message>
    <message>
        <source>puzzle-game-btn</source>
        <translation type="vanished">Juego de rompecabezas</translation>
    </message>
</context>
<context>
    <name>PeerSettingsPane</name>
    <message>
        <source>address-label</source>
        <translation>Dirección</translation>
    </message>
    <message>
        <source>copy-btn</source>
        <translation>Copiar</translation>
    </message>
    <message>
        <source>copied-to-clipboard-notification</source>
        <extracomment>notification: copied to clipboard</extracomment>
        <translation>Copiado al portapapeles</translation>
    </message>
    <message>
        <source>display-name-label</source>
        <translation>Nombre de Usuario</translation>
    </message>
    <message>
        <source>save-btn</source>
        <translation>Guardar</translation>
    </message>
    <message>
        <source>block-btn</source>
        <translation>Bloquear contacto</translation>
    </message>
    <message>
        <source>save-peer-history</source>
        <extracomment>Save Peer History</extracomment>
        <translation>Guardar el historial con contacto</translation>
    </message>
    <message>
        <source>save-peer-history-description</source>
        <translation>Determina si eliminar o no el historial asociado con el contacto.</translation>
    </message>
    <message>
        <source>dont-save-peer-history</source>
        <translation>Eliminar historial de contacto</translation>
    </message>
    <message>
        <source>unblock-btn</source>
        <translation type="vanished">Desbloquear contacto</translation>
    </message>
    <message>
        <source>delete-btn</source>
        <translation>Eliminar</translation>
    </message>
</context>
<context>
    <name>ProfileAddEditPane</name>
    <message>
        <source>add-profile-title</source>
        <translation type="vanished">Agregar nuevo perfil</translation>
    </message>
    <message>
        <source>edit-profile-title</source>
        <translation type="vanished">Editar perfil</translation>
    </message>
    <message>
        <source>profile-name</source>
        <translation type="vanished">Nombre de Usuario</translation>
    </message>
    <message>
        <source>default-profile-name</source>
        <translation type="vanished">Alicia</translation>
    </message>
    <message>
        <source>new-profile</source>
        <extracomment>New Profile || Edit Profile</extracomment>
        <translation>Nuevo perfil</translation>
    </message>
    <message>
        <source>edit-profile</source>
        <translation>Editar perfil</translation>
    </message>
    <message>
        <source>profile-onion-label</source>
        <translation type="vanished">Envía esta dirección a los contactos con los que quieras conectarte</translation>
    </message>
    <message>
        <source>copy-btn</source>
        <translation>Copiar</translation>
    </message>
    <message>
        <source>copied-to-clipboard-notification</source>
        <extracomment>notification: copied to clipboard</extracomment>
        <translation>Copiado al portapapeles</translation>
    </message>
    <message>
        <source>radio-use-password</source>
        <extracomment>Password</extracomment>
        <translation>Contraseña</translation>
    </message>
    <message>
        <source>radio-no-password</source>
        <extracomment>Unencrypted (No password)</extracomment>
        <translation>Sin cifrado (sin contraseña)</translation>
    </message>
    <message>
        <source>no-password-warning</source>
        <extracomment>Not using a password on this account means that all data stored locally will not be encrypted</extracomment>
        <translation>No usar una contraseña para esta cuenta significa que los datos almacenados localmente no serán encriptados</translation>
    </message>
    <message>
        <source>profile-oniblon-label</source>
        <extracomment>Send this address to peers you want to connect with</extracomment>
        <translation>Envía esta dirección a los contactos con los que quieras conectarte</translation>
    </message>
    <message>
        <source>your-display-name</source>
        <extracomment>Your Display Name</extracomment>
        <translation>Tu nombre de usuario</translation>
    </message>
    <message>
        <source>current-password-label</source>
        <extracomment>Current Password</extracomment>
        <translation>Contraseña actual</translation>
    </message>
    <message>
        <source>password1-label</source>
        <extracomment>Password</extracomment>
        <translation>Contraseña</translation>
    </message>
    <message>
        <source>password2-label</source>
        <extracomment>Reenter password</extracomment>
        <translation>Vuelve a ingresar tu contraseña</translation>
    </message>
    <message>
        <source>password-error-empty</source>
        <extracomment>Passwords do not match</extracomment>
        <translation>El campo de contraseña no puede estar vacío</translation>
    </message>
    <message>
        <source>create-profile-btn</source>
        <extracomment>Create || Save</extracomment>
        <translation>Crear perfil</translation>
    </message>
    <message>
        <source>save-profile-btn</source>
        <translation>Guardar perfil</translation>
    </message>
    <message>
        <source>password-error-match</source>
        <translation>Las contraseñas no coinciden</translation>
    </message>
    <message>
        <source>password-change-error</source>
        <extracomment>Error changing password: Supplied password rejected</extracomment>
        <translation>Hubo un error cambiando tu contraseña: la contraseña ingresada fue rechazada</translation>
    </message>
    <message>
        <source>delete-profile-btn</source>
        <extracomment>Delete Profile</extracomment>
        <translation>Eliminar Perfil</translation>
    </message>
    <message>
        <source>delete-confirm-label</source>
        <extracomment>Type DELETE to confirm</extracomment>
        <translation>Escribe ELIMINAR para confirmar</translation>
    </message>
    <message>
        <source>delete-profile-confirm-btn</source>
        <extracomment>Really Delete Profile</extracomment>
        <translation>Confirmar eliminar perfil</translation>
    </message>
    <message>
        <source>delete-confirm-text</source>
        <extracomment>DELETE</extracomment>
        <translation>ELIMINAR</translation>
    </message>
</context>
<context>
    <name>ProfileList</name>
    <message>
        <source>add-new-profile-btn</source>
        <translation>Agregar nuevo perfil</translation>
    </message>
</context>
<context>
    <name>ProfileManagerPane</name>
    <message>
        <source>enter-profile-password</source>
        <extracomment>Enter a password to view your profiles</extracomment>
        <translation>Ingresa tu contraseña para ver tus perfiles</translation>
    </message>
    <message>
        <source>password</source>
        <translation>Contraseña</translation>
    </message>
    <message>
        <source>error-0-profiles-loaded-for-password</source>
        <extracomment>0 profiles loaded with that password</extracomment>
        <translation type="vanished">0 perfiles cargados con esa contraseña</translation>
    </message>
    <message>
        <source>your-profiles</source>
        <extracomment>Your Profiles</extracomment>
        <translation>Tus perfiles</translation>
    </message>
    <message>
        <source>your-servers</source>
        <extracomment>Your Profiles</extracomment>
        <translation>Tus servidores</translation>
    </message>
    <message>
        <source>unlock</source>
        <extracomment>Unlock</extracomment>
        <translation>Desbloquear</translation>
    </message>
</context>
<context>
    <name>SettingsPane</name>
    <message>
        <source>cwtch-settings-title</source>
        <translation type="vanished">Configuración de Cwtch</translation>
    </message>
    <message>
        <source>version %1 builddate %2</source>
        <translation type="vanished">Versión: %1 Basado en %2</translation>
    </message>
    <message>
        <source>zoom-label</source>
        <translation type="vanished">Zoom de la interfaz (afecta principalmente el tamaño del texto y de los botones)</translation>
    </message>
    <message>
        <source>block-unknown-label</source>
        <translation type="vanished">Bloquear conexiones desconocidas</translation>
    </message>
    <message>
        <source>setting-language</source>
        <extracomment>Language</extracomment>
        <translation>Idioma</translation>
    </message>
    <message>
        <source>locale-en</source>
        <translation>Inglés</translation>
    </message>
    <message>
        <source>locale-fr</source>
        <translation>Francés</translation>
    </message>
    <message>
        <source>locale-pt</source>
        <translation>Portugués</translation>
    </message>
    <message>
        <source>locale-de</source>
        <translation>Alemán</translation>
    </message>
    <message>
        <source>setting-interface-zoom</source>
        <extracomment>Interface Zoom</extracomment>
        <translation>Nivel de zoom</translation>
    </message>
    <message>
        <source>large-text-label</source>
        <translation>Grande</translation>
    </message>
    <message>
        <source>setting-theme</source>
        <extracomment>Theme</extracomment>
        <translation>Tema</translation>
    </message>
    <message>
        <source>theme-light</source>
        <translation>Claro</translation>
    </message>
    <message>
        <source>theme-dark</source>
        <translation>Oscuro</translation>
    </message>
    <message>
        <source>experiments-enabled</source>
        <extracomment>Theme</extracomment>
        <translation>Experimentos habilitados</translation>
    </message>
    <message>
        <source>version %1 tor %2</source>
        <extracomment>Version %1 with tor %2</extracomment>
        <translation>Versión %1 con tor %2</translation>
    </message>
    <message>
        <source>version %1</source>
        <translation type="vanished">Versión %1</translation>
    </message>
    <message>
        <source>builddate %2</source>
        <extracomment>Built on: %2</extracomment>
        <translation>Basado en: %2</translation>
    </message>
    <message>
        <source>default-scaling-text</source>
        <translation type="vanished">Tamaño predeterminado de texto (factor de escala:</translation>
    </message>
    <message>
        <source>small-text-label</source>
        <translation>Pequeño</translation>
    </message>
    <message>
        <source>locale-es</source>
        <translation>Español</translation>
    </message>
    <message>
        <source>locale-it</source>
        <translation>Italiano</translation>
    </message>
</context>
<context>
    <name>SplashPane</name>
    <message>
        <source>loading-tor</source>
        <extracomment>Loading tor...</extracomment>
        <translation>Cargando tor...</translation>
    </message>
</context>
<context>
    <name>StackToolbar</name>
    <message>
        <source>view-group-membership-tooltip</source>
        <translation type="vanished">Ver membresía del grupo</translation>
    </message>
</context>
<context>
    <name>Statusbar</name>
    <message>
        <source>network-status-disconnected</source>
        <extracomment>Disconnected from the internet, check your connection</extracomment>
        <translation>Sin conexión, comprueba tu conexión</translation>
    </message>
    <message>
        <source>network-status-attempting-tor</source>
        <extracomment>Attempting to connect to Tor network</extracomment>
        <translation>Intentando conectarse a la red Tor</translation>
    </message>
    <message>
        <source>network-status-connecting</source>
        <extracomment>Connecting...</extracomment>
        <translation>Conectando a la red y a los contactos...</translation>
    </message>
    <message>
        <source>network-status-online</source>
        <extracomment>Online</extracomment>
        <translation>En línea</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <source>new-connection-pane-title</source>
        <extracomment>New Connection</extracomment>
        <translation>Nueva conexión</translation>
    </message>
    <message>
        <source>error-0-profiles-loaded-for-password</source>
        <extracomment>0 profiles loaded with that password</extracomment>
        <translation>0 perfiles cargados con esa contraseña</translation>
    </message>
</context>
</TS>
