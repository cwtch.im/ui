<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="it">
<context>
    <name>AddGroupPane</name>
    <message>
        <source>create-group-title</source>
        <translation type="vanished">Crea un gruppo</translation>
    </message>
    <message>
        <source>server-label</source>
        <translation type="vanished">Server</translation>
    </message>
    <message>
        <source>group-name-label</source>
        <translation type="vanished">Nome del gruppo</translation>
    </message>
    <message>
        <source>default-group-name</source>
        <translation type="vanished">Gruppo fantastico</translation>
    </message>
    <message>
        <source>create-group-btn</source>
        <translation type="vanished">Crea</translation>
    </message>
</context>
<context>
    <name>AddPeerGroupPane</name>
    <message>
        <source>profile-oniblon-label</source>
        <extracomment>Send this address to peers you want to connect with</extracomment>
        <translation>Inviare questo indirizzo ai peer con cui si desidera connettersi</translation>
    </message>
    <message>
        <source>copy-btn</source>
        <translation>Copia</translation>
    </message>
    <message>
        <source>copied-to-clipboard-notification</source>
        <extracomment>notification: copied to clipboard</extracomment>
        <translation>Copiato negli Appunti</translation>
    </message>
    <message>
        <source>add-peer-tab</source>
        <translation>Aggiungi un peer</translation>
    </message>
    <message>
        <source>create-group-tab</source>
        <translation>Crea un gruppo</translation>
    </message>
    <message>
        <source>join-group-tab</source>
        <translation>Unisciti a un gruppo</translation>
    </message>
    <message>
        <source>peer-address</source>
        <extracomment>Address</extracomment>
        <translation>Indirizzo</translation>
    </message>
    <message>
        <source>peer-name</source>
        <extracomment>Name</extracomment>
        <translation>Nome</translation>
    </message>
    <message>
        <source>group-name</source>
        <extracomment>Group Name</extracomment>
        <translation>Nome del gruppo</translation>
    </message>
    <message>
        <source>server</source>
        <extracomment>Server</extracomment>
        <translation>Server</translation>
    </message>
    <message>
        <source>invitation</source>
        <translation type="vanished">Invito</translation>
    </message>
    <message>
        <source>group-addr</source>
        <extracomment>Address</extracomment>
        <translation>Indirizzo</translation>
    </message>
    <message>
        <source>add-peer</source>
        <translation type="vanished">Aggiungi peer</translation>
    </message>
    <message>
        <source>create-group</source>
        <translation type="vanished">Crea un gruppo</translation>
    </message>
    <message>
        <source>join-group</source>
        <translation type="vanished">Unisciti al gruppo</translation>
    </message>
</context>
<context>
    <name>BulletinOverlay</name>
    <message>
        <source>new-bulletin-label</source>
        <translation>Nuovo bollettino</translation>
    </message>
    <message>
        <source>post-new-bulletin-label</source>
        <extracomment>Post a new Bulletin Post</extracomment>
        <translation>Pubblica un nuovo bollettino</translation>
    </message>
    <message>
        <source>title-placeholder</source>
        <extracomment>title place holder text</extracomment>
        <translation>titolo...</translation>
    </message>
</context>
<context>
    <name>ChatOverlay</name>
    <message>
        <source>chat-history-default</source>
        <extracomment>This conversation will be deleted when Cwtch is closed! Message history can be enabled per-conversation via the Settings menu in the upper right.</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>chat-history-disabled</source>
        <extracomment>Message history is disabled.</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>chat-history-enabled</source>
        <extracomment>Message history is enabled.</extracomment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ContactList</name>
    <message>
        <source>paste-address-to-add-contact</source>
        <translation type="vanished">... incolla qui un indirizzo per aggiungere un contatto...</translation>
    </message>
    <message>
        <source>blocked</source>
        <translation>Bloccato</translation>
    </message>
</context>
<context>
    <name>EmojiDrawer</name>
    <message>
        <source>cycle-cats-android</source>
        <translation type="vanished">Fare clic per scorrere le categorie.
Pressione lunga per resettare.</translation>
    </message>
    <message>
        <source>cycle-cats-desktop</source>
        <translation type="vanished">Fare clic per scorrere le categorie.
Cliccare con il tasto destro per resettare.</translation>
    </message>
    <message>
        <source>cycle-morphs-android</source>
        <translation type="vanished">Fare clic per scorrere i morph.
Pressione lunga per resettare.</translation>
    </message>
    <message>
        <source>cycle-morphs-desktop</source>
        <translation type="vanished">Fare clic per scorrere i morph.
Cliccare con il tasto destro per resettare.</translation>
    </message>
    <message>
        <source>cycle-colours-android</source>
        <translation type="vanished">Fare clic per scorrere i colori.
Pressione lunga per resettare.</translation>
    </message>
    <message>
        <source>cycle-colours-desktop</source>
        <translation type="vanished">Fare clic per scorrere i colori.
Cliccare con il tasto destro per resettare.</translation>
    </message>
    <message>
        <source>search</source>
        <translation type="vanished">Ricerca...</translation>
    </message>
</context>
<context>
    <name>GroupSettingsPane</name>
    <message>
        <source>server-label</source>
        <translation>Server</translation>
    </message>
    <message>
        <source>copy-btn</source>
        <translation>Copia</translation>
    </message>
    <message>
        <source>copied-to-clipboard-notification</source>
        <extracomment>notification: copied to clipboard</extracomment>
        <translation>Copiato negli Appunti</translation>
    </message>
    <message>
        <source>invitation-label</source>
        <translation>Invito</translation>
    </message>
    <message>
        <source>server-info</source>
        <translation>Informazioni sul server</translation>
    </message>
    <message>
        <source>server-connectivity-connected</source>
        <translation>Server connesso</translation>
    </message>
    <message>
        <source>server-connectivity-disconnected</source>
        <translation>Server disconnesso</translation>
    </message>
    <message>
        <source>server-synced</source>
        <translation>Sincronizzato</translation>
    </message>
    <message>
        <source>server-not-synced</source>
        <translation>Non sincronizzato</translation>
    </message>
    <message>
        <source>view-server-info</source>
        <translation>Informazioni sul server</translation>
    </message>
    <message>
        <source>group-name-label</source>
        <translation>Nome del gruppo</translation>
    </message>
    <message>
        <source>save-btn</source>
        <translation>Salva</translation>
    </message>
    <message>
        <source>invite-to-group-label</source>
        <translation type="vanished">Invitare nel gruppo</translation>
    </message>
    <message>
        <source>invite-btn</source>
        <translation type="vanished">Invitare</translation>
    </message>
    <message>
        <source>delete-btn</source>
        <translation>Elimina</translation>
    </message>
</context>
<context>
    <name>InplaceEditText</name>
    <message>
        <source>Update</source>
        <translation type="vanished">Aggiornamento</translation>
    </message>
</context>
<context>
    <name>ListOverlay</name>
    <message>
        <source>search-list</source>
        <extracomment>ex: &quot;Find...&quot;</extracomment>
        <translation>Cerca nella lista</translation>
    </message>
    <message>
        <source>peer-not-online</source>
        <translation>Il peer è offline. Le applicazioni non possono essere utilizzate in questo momento.</translation>
    </message>
    <message>
        <source>add-list-item-btn</source>
        <translation>Aggiungi elemento</translation>
    </message>
    <message>
        <source>add-list-item</source>
        <translation type="vanished">Aggiungi un nuovo elemento alla lista</translation>
    </message>
    <message>
        <source>add-new-item</source>
        <translation type="vanished">Aggiungi un nuovo elemento alla lista</translation>
    </message>
    <message>
        <source>todo-placeholder</source>
        <translation type="vanished">Da fare...</translation>
    </message>
</context>
<context>
    <name>MembershipOverlay</name>
    <message>
        <source>membership-description</source>
        <extracomment>Below is a list of users who have sent messages to the group. This list may not reflect all users who have access to the group.</extracomment>
        <translation>Di seguito è riportato un elenco di utenti che hanno inviato messaggi al gruppo. Questo elenco potrebbe non corrispondere a tutti gli utenti che hanno accesso al gruppo.</translation>
    </message>
</context>
<context>
    <name>Message</name>
    <message>
        <source>dm-tooltip</source>
        <extracomment>Click to DM</extracomment>
        <translation>Clicca per inviare un Messagio Diretto</translation>
    </message>
    <message>
        <source>could-not-send-msg-error</source>
        <extracomment>Could not send this message</extracomment>
        <translation>Impossibile inviare questo messaggio</translation>
    </message>
    <message>
        <source>acknowledged-label</source>
        <translation>Riconosciuto</translation>
    </message>
    <message>
        <source>pending-label</source>
        <translation>In corso</translation>
    </message>
</context>
<context>
    <name>MessageEditor</name>
    <message>
        <source>peer-blocked-message</source>
        <extracomment>Peer is blocked</extracomment>
        <translation>Il peer è bloccato</translation>
    </message>
    <message>
        <source>peer-offline-message</source>
        <extracomment>Peer is offline, messages can&apos;t be delivered right now</extracomment>
        <translation>Il peer è offline, i messaggi non possono essere recapitati in questo momento</translation>
    </message>
</context>
<context>
    <name>MyProfile</name>
    <message>
        <source>copy-btn</source>
        <translation type="vanished">Copia</translation>
    </message>
    <message>
        <source>copied-clipboard-notification</source>
        <translation type="vanished">Copiato negli Appunti</translation>
    </message>
    <message>
        <source>new-group-btn</source>
        <translation type="vanished">Crea un nuovo gruppo</translation>
    </message>
    <message>
        <source>paste-address-to-add-contact</source>
        <translation type="vanished">... incolla qui un indirizzo per aggiungere un contatto ...</translation>
    </message>
</context>
<context>
    <name>OverlayPane</name>
    <message>
        <source>accept-group-invite-label</source>
        <translation type="vanished">Vuoi accettare l&apos;invito a</translation>
    </message>
    <message>
        <source>accept-group-btn</source>
        <translation type="vanished">Accetta</translation>
    </message>
    <message>
        <source>reject-group-btn</source>
        <translation type="vanished">Rifiuta</translation>
    </message>
    <message>
        <source>chat-btn</source>
        <translation>Chat</translation>
    </message>
    <message>
        <source>lists-btn</source>
        <translation>Liste</translation>
    </message>
    <message>
        <source>bulletins-btn</source>
        <translation>Bollettini</translation>
    </message>
    <message>
        <source>puzzle-game-btn</source>
        <translation type="vanished">Gioco di puzzle</translation>
    </message>
</context>
<context>
    <name>PeerSettingsPane</name>
    <message>
        <source>address-label</source>
        <translation>Indirizzo</translation>
    </message>
    <message>
        <source>copy-btn</source>
        <translation>Copia</translation>
    </message>
    <message>
        <source>copied-to-clipboard-notification</source>
        <extracomment>notification: copied to clipboard</extracomment>
        <translation>Copiato negli Appunti</translation>
    </message>
    <message>
        <source>display-name-label</source>
        <translation>Nome visualizzato</translation>
    </message>
    <message>
        <source>save-btn</source>
        <translation>Salva</translation>
    </message>
    <message>
        <source>block-btn</source>
        <translation>Blocca il peer</translation>
    </message>
    <message>
        <source>save-peer-history</source>
        <extracomment>Save Peer History</extracomment>
        <translation>Salva cronologia peer</translation>
    </message>
    <message>
        <source>save-peer-history-description</source>
        <translation>Determina se eliminare o meno ogni cronologia eventualmente associata al peer.</translation>
    </message>
    <message>
        <source>dont-save-peer-history</source>
        <translation>Elimina cronologia dei peer</translation>
    </message>
    <message>
        <source>unblock-btn</source>
        <translation type="vanished">Sblocca il peer</translation>
    </message>
    <message>
        <source>delete-btn</source>
        <translation>Elimina</translation>
    </message>
</context>
<context>
    <name>ProfileAddEditPane</name>
    <message>
        <source>add-profile-title</source>
        <translation type="vanished">Aggiungi nuovo profilo</translation>
    </message>
    <message>
        <source>edit-profile-title</source>
        <translation type="vanished">Modifica profilo</translation>
    </message>
    <message>
        <source>profile-name</source>
        <translation type="vanished">Nome visualizzato</translation>
    </message>
    <message>
        <source>default-profile-name</source>
        <translation type="vanished">Alice</translation>
    </message>
    <message>
        <source>new-profile</source>
        <extracomment>New Profile || Edit Profile</extracomment>
        <translation>Nuovo profilo</translation>
    </message>
    <message>
        <source>edit-profile</source>
        <translation>Modifica profilo</translation>
    </message>
    <message>
        <source>profile-onion-label</source>
        <translation type="vanished">Inviare questo indirizzo ai peer con cui si desidera connettersi</translation>
    </message>
    <message>
        <source>copy-btn</source>
        <translation>Copia</translation>
    </message>
    <message>
        <source>copied-to-clipboard-notification</source>
        <extracomment>notification: copied to clipboard</extracomment>
        <translation>Copiato negli appunti</translation>
    </message>
    <message>
        <source>radio-use-password</source>
        <extracomment>Password</extracomment>
        <translation>Password</translation>
    </message>
    <message>
        <source>radio-no-password</source>
        <extracomment>Unencrypted (No password)</extracomment>
        <translation>Non criptato (senza password)</translation>
    </message>
    <message>
        <source>no-password-warning</source>
        <extracomment>Not using a password on this account means that all data stored locally will not be encrypted</extracomment>
        <translation>Non utilizzare una password su questo account significa che tutti i dati archiviati localmente non verranno criptati</translation>
    </message>
    <message>
        <source>profile-oniblon-label</source>
        <extracomment>Send this address to peers you want to connect with</extracomment>
        <translation>Inviare questo indirizzo ai peer con cui si desidera connettersi</translation>
    </message>
    <message>
        <source>your-display-name</source>
        <extracomment>Your Display Name</extracomment>
        <translation>Il tuo nome visualizzato</translation>
    </message>
    <message>
        <source>current-password-label</source>
        <extracomment>Current Password</extracomment>
        <translation>Password corrente</translation>
    </message>
    <message>
        <source>password1-label</source>
        <extracomment>Password</extracomment>
        <translation>Password</translation>
    </message>
    <message>
        <source>password2-label</source>
        <extracomment>Reenter password</extracomment>
        <translation>Reinserire la password</translation>
    </message>
    <message>
        <source>password-error-empty</source>
        <extracomment>Passwords do not match</extracomment>
        <translation>La password non può essere vuota</translation>
    </message>
    <message>
        <source>create-profile-btn</source>
        <extracomment>Create || Save</extracomment>
        <translation>Crea un profilo</translation>
    </message>
    <message>
        <source>save-profile-btn</source>
        <translation>Salva il profilo</translation>
    </message>
    <message>
        <source>password-error-match</source>
        <translation>Le password non corrispondono</translation>
    </message>
    <message>
        <source>password-change-error</source>
        <extracomment>Error changing password: Supplied password rejected</extracomment>
        <translation>Errore durante la modifica della password: password fornita rifiutata</translation>
    </message>
    <message>
        <source>delete-profile-btn</source>
        <extracomment>Delete Profile</extracomment>
        <translation>Elimina profilo</translation>
    </message>
    <message>
        <source>delete-confirm-label</source>
        <extracomment>Type DELETE to confirm</extracomment>
        <translation>Digita ELIMINA per confermare</translation>
    </message>
    <message>
        <source>delete-profile-confirm-btn</source>
        <extracomment>Really Delete Profile</extracomment>
        <translation>Elimina realmente il profilo</translation>
    </message>
    <message>
        <source>delete-confirm-text</source>
        <extracomment>DELETE</extracomment>
        <translation>ELIMINA</translation>
    </message>
</context>
<context>
    <name>ProfileList</name>
    <message>
        <source>add-new-profile-btn</source>
        <translation>Aggiungi nuovo profilo</translation>
    </message>
</context>
<context>
    <name>ProfileManagerPane</name>
    <message>
        <source>enter-profile-password</source>
        <extracomment>Enter a password to view your profiles</extracomment>
        <translation>Inserisci una password per visualizzare i tuoi profili</translation>
    </message>
    <message>
        <source>password</source>
        <translation>Password</translation>
    </message>
    <message>
        <source>error-0-profiles-loaded-for-password</source>
        <translation type="vanished">0 profili caricati con quella password</translation>
    </message>
    <message>
        <source>your-profiles</source>
        <extracomment>Your Profiles</extracomment>
        <translation>I tuoi profili</translation>
    </message>
    <message>
        <source>your-servers</source>
        <extracomment>Your Profiles</extracomment>
        <translation>I tuoi server</translation>
    </message>
    <message>
        <source>unlock</source>
        <extracomment>Unlock</extracomment>
        <translation>Sblocca</translation>
    </message>
</context>
<context>
    <name>SettingsPane</name>
    <message>
        <source>cwtch-settings-title</source>
        <translation type="vanished">Impostazioni di Cwtch</translation>
    </message>
    <message>
        <source>version %1 builddate %2</source>
        <translation type="vanished">Versione: %1 Costruito il: %2</translation>
    </message>
    <message>
        <source>zoom-label</source>
        <translation type="vanished">Zoom dell&apos;interfaccia (influisce principalmente sulle dimensioni del testo e dei pulsanti)</translation>
    </message>
    <message>
        <source>block-unknown-label</source>
        <translation type="vanished">Blocca peer sconosciuti</translation>
    </message>
    <message>
        <source>setting-language</source>
        <extracomment>Language</extracomment>
        <translation>Lingua</translation>
    </message>
    <message>
        <source>locale-en</source>
        <translation>Inglese</translation>
    </message>
    <message>
        <source>locale-fr</source>
        <translation>Francese</translation>
    </message>
    <message>
        <source>locale-pt</source>
        <translation>Portoghese</translation>
    </message>
    <message>
        <source>locale-de</source>
        <translation>Tedesco</translation>
    </message>
    <message>
        <source>setting-interface-zoom</source>
        <extracomment>Interface Zoom</extracomment>
        <translation>Livello di zoom</translation>
    </message>
    <message>
        <source>large-text-label</source>
        <translation>Grande</translation>
    </message>
    <message>
        <source>setting-theme</source>
        <extracomment>Theme</extracomment>
        <translation>Tema</translation>
    </message>
    <message>
        <source>theme-light</source>
        <translation>Chiaro</translation>
    </message>
    <message>
        <source>theme-dark</source>
        <translation>Scuro</translation>
    </message>
    <message>
        <source>experiments-enabled</source>
        <extracomment>Theme</extracomment>
        <translation>Esperimenti abilitati</translation>
    </message>
    <message>
        <source>version %1 tor %2</source>
        <extracomment>Version %1 with tor %2</extracomment>
        <translation>Versione %1 con tor %2</translation>
    </message>
    <message>
        <source>version %1</source>
        <translation type="vanished">Versione %1</translation>
    </message>
    <message>
        <source>builddate %2</source>
        <extracomment>Built on: %2</extracomment>
        <translation>Costruito il: %2</translation>
    </message>
    <message>
        <source>default-scaling-text</source>
        <translation type="vanished">Testo di dimensioni predefinite (fattore di scala:</translation>
    </message>
    <message>
        <source>small-text-label</source>
        <translation>Piccolo</translation>
    </message>
    <message>
        <source>locale-es</source>
        <translation>Spagnolo</translation>
    </message>
    <message>
        <source>locale-it</source>
        <translation>Italiano</translation>
    </message>
</context>
<context>
    <name>SplashPane</name>
    <message>
        <source>loading-tor</source>
        <extracomment>Loading tor...</extracomment>
        <translation>Caricamento di tor...</translation>
    </message>
</context>
<context>
    <name>StackToolbar</name>
    <message>
        <source>view-group-membership-tooltip</source>
        <translation type="vanished">Visualizza i membri del gruppo</translation>
    </message>
</context>
<context>
    <name>Statusbar</name>
    <message>
        <source>network-status-disconnected</source>
        <extracomment>Disconnected from the internet, check your connection</extracomment>
        <translation>Disconnesso da Internet, controlla la tua connessione</translation>
    </message>
    <message>
        <source>network-status-attempting-tor</source>
        <extracomment>Attempting to connect to Tor network</extracomment>
        <translation>Tentativo di connessione alla rete Tor</translation>
    </message>
    <message>
        <source>network-status-connecting</source>
        <extracomment>Connecting...</extracomment>
        <translation>Connessione alla rete e ai peer ...</translation>
    </message>
    <message>
        <source>network-status-online</source>
        <extracomment>Online</extracomment>
        <translation>Online</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <source>new-connection-pane-title</source>
        <extracomment>New Connection</extracomment>
        <translation>Nuova connessione</translation>
    </message>
    <message>
        <source>error-0-profiles-loaded-for-password</source>
        <extracomment>0 profiles loaded with that password</extracomment>
        <translation>0 profili caricati con quella password</translation>
    </message>
</context>
</TS>
