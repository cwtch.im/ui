#!/bin/sh

mkdir -p ~/.local/bin
cp ui ~/.local/bin/cwtch

mkdir -p ~/.local/share/icons
cp cwtch.png ~/.local/share/icons

mkdir -p ~/.local/share/cwtch
cp -r assets ~/.local/share/cwtch

cp -r icons ~/.local/share/

mkdir -p ~/.local/share/applications
sed "s|~|$HOME|" cwtch.desktop > $HOME/.local/share/applications/cwtch.desktop
