pragma Singleton

import QtQuick 2.0

Item {

    // defined in cwtch.im/cwtch/model/profile.go ln24
    readonly property string auth_unknown: "unknown"
    readonly property string auth_blocked: "blocked"
    readonly property string auth_approved: "approved"

    // defined in cwtch.im/ui/go/constants/attributes.go
    readonly property string show_blocked: "show-blocked"

    // defined in cwtch.im/cwtch/protocol/connection/state.go
    //0:Disconnected,1:Connecting,2:Connected,3:Authenticated,4:Synced,5:Failed,6:Killed
    readonly property int state_disconnected: 0
    readonly property int state_connecting: 1
    readonly property int state_connected: 2
    readonly property int state_authenticated: 3
    readonly property int state_synced: 4
    readonly property int state_failed: 5
    readonly property int state_killed: 6

    // Tor statuses
    readonly property int statusDisconnectedInternet: 0
    readonly property int statusDisconnectedTor: 1
    readonly property int statusConnecting: 2
    readonly property int statusOnline: 3


}
