import QtGraphicalEffects 1.0
import QtQuick 2.7
import QtQuick.Controls 2.4
import QtQuick.Controls.Material 2.0
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.3


import "../opaque" as Opaque
import "../opaque/styles"
import "../opaque/theme"
import "../utils.js" as Utils

ColumnLayout {
    Layout.fillWidth: true
    width:parent.width

    Text {
        Layout.fillWidth: true
    }

    TextField {
        id: filter

        placeholderText: "Search.."

        style: CwtchTextFieldStyle{}

        anchors.left: parent.left
        anchors.right: parent.right

        anchors.margins: 10

        onTextChanged: {
            bulletinView.filter = text
            if (bulletinView.model.get(bulletinView.currentIndex).title.indexOf(text) == -1) {
                bulletinView.currentIndex = -1
            }
        }
    }

    Flickable { // THE MESSAGE LIST ITSELF
        id: sv
        clip: true
        Layout.alignment: Qt.AlignLeft | Qt.AlignTop
        Layout.fillHeight: true
        Layout.fillWidth: true
        contentWidth: parent.width
        contentHeight: parent.height
        boundsBehavior: Flickable.StopAtBounds
        maximumFlickVelocity: 800


		Connections {
			id: cnxns1
			target: mm
			onRowsInserted: {
				var msg = mm.getMessage(first);
				var name = msg.peerID == gcd.selectedProfile ? "me" : mm.getNick(msg.peerID);
				cnxns2.handler(msg.peerID, msg.peerID, name, msg.rawMessage, mm.getImage(msg.peerID), msg.signature, msg.peerID == gcd.selectedProfile, msg.timestamp, msg.ack, msg.error)
			}
		}

        Connections {
			id: cnxns2
            target: gcd

            onClearMessages: function() {
                jsonModel4.clear()
            }

            onAppendMessage: function(handle, from, displayName, message, image, mid, fromMe, ts, ack, error) {
                handler(handle, from, displayName, message, image, mid, fromMe, ts, ack, error)
            }

            onPrependMessage: function(handle, from, displayName, message, image, mid, fromMe, ts, ack, error) {
                handler(handle, from, displayName, message, image, mid, fromMe, ts, ack, error)
            }

            function handler(handle, from, displayName, message, image, mid, fromMe, ts, ack, error) {
                var msg
                try {
                    msg = JSON.parse(message)
                } catch (e) {
                    return
                }
                if (msg.o != 2) return

                if (msg.t != undefined && msg.b != undefined) {
                    jsonModel4.insert(0,{
                        "title":msg.t,
                        "body": msg.b,
                        "selected":false,
                        "from": from,
                        "displayName": displayName,
                        "timestamp": ts
                    })
                }

                /*if (sv.contentY + sv.height >= sv.contentHeight - colMessages.height && sv.contentHeight > sv.height) {
                sv.contentY = sv.contentHeight - sv.height
                }*/
            }

            onUpdateContactStatus: function(_handle, _status, _loading) {
                if (gcd.selectedConversation == _handle) {
                    if (_loading == true) {
                        newposttitle.enabled = false
                        newpostbody.enabled = false
                        btnSend.enabled = false
                    } else {
                        newposttitle.enabled = true
                        newpostbody.enabled = true
                        btnSend.enabled = true
                    }

                }
            }
        }

        ScrollBar.vertical: ScrollBar{
            policy: ScrollBar.AlwaysOn
        }

        ListView {
            id: bulletinView
            anchors.left: parent.left
            anchors.leftMargin: 10
            anchors.topMargin: 10

            width: parent.width - 50
            height: parent.height - 20
            orientation: Qt.Vertical
            spacing: 10
            model: jsonModel4
            property string filter: ""
            delegate:

            Item {
                width: parent.width
                height:  title.indexOf(bulletinView.filter) >= 0 ? (selected ? texttitle.height + textbody.height + replybtn.height + 8 : texttitle.height * 2) : 0
                visible: title.indexOf(bulletinView.filter) >= 0

                Column {
                    width: parent.width

                    RowLayout {
                        Button {
                            text: selected  ? "-" : "+"
                            style: CwtchExpandingButton{}
                        }
                        Text {
                            id: texttitle
                            text: '<b>' + Utils.htmlEscaped(title) + '</b> by ' + displayName + "<br/>" + Qt.formatDateTime(new Date(timestamp*1000), "MMMM d, h:mm ap")
                            leftPadding: 10
                            topPadding: 5
                            bottomPadding:5
                            color: Theme.mainTextColor
                        }
                        MouseArea {
                            anchors.fill: parent
                            onClicked: {
                                selected = !selected
                                bulletinView.currentIndex = index
                            }

                        }
                    }

                    Rectangle {
                        height: 1
                        color: windowItem.cwtch_color
                        anchors {
                            left: parent.left
                            right: parent.right
                        }
                    }

                    Text {
                        id: textbody
                        visible: selected
                        text: Utils.htmlEscaped(body)
                        wrapMode: TextEdit.Wrap
                        leftPadding: 10
                        topPadding: 10
                        width: parent.width - 50
                        color: Theme.mainTextColor
                    }

                    Opaque.Button {
                        id: replybtn
                        visible: selected
                        text: "reply"
                        anchors.right: parent.right
                        anchors.rightMargin:10
                        onClicked: {
                            gcd.broadcast("ResetMessagePane")
                            theStack.pane = theStack.messagePane
                            gcd.loadMessagesPane(from)
                            overlayStack.overlay = overlayStack.chatOverlay
                        }
                    }
                }

            }

            focus: true
            ListModel {
                id: jsonModel4
            }
        }
    }

    GroupBox {
        title: qsTr("new-bulletin-label")
        Layout.fillWidth: true

        RowLayout {
            Layout.fillWidth: true
            width: parent.width
            ColumnLayout {
                Layout.fillWidth: true


                Text {
                    //: Post a new Bulletin Post
                    text: qsTr("post-new-bulletin-label")
        			color: Theme.mainTextColor
                }

                TextField {
                    id: newposttitle
                    //: title place holder text
                    placeholderText: qsTr("title-placeholder")
                    Layout.fillWidth: true
                    style: CwtchTextFieldStyle{}
                }

                TextArea {
                    id: newpostbody
                    Layout.fillWidth: true
                    style: CwtchTextAreaStyle{}
                }


                Opaque.Button { // SEND MESSAGE BUTTON
                    id: btnSend
                    icon: "regular/paper-plane"
                    text: "post"
                    anchors.right: parent.right
                    anchors.rightMargin: 2

                    property int nextMessageID: 1

                    onClicked: {
                        if (newposttitle.text != "" && newpostbody.text != "") {
                            var msg = JSON.stringify({"o":2, "t":newposttitle.text, "b":newpostbody.text})
                            gcd.sendMessage(msg, nextMessageID++)
                        }
                        newposttitle.text = ""
                        newpostbody.text = ""
                    }
                }
            }
        }

    }






}
