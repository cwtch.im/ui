import QtGraphicalEffects 1.0
import QtQuick 2.7
import QtQuick.Controls 2.4
import QtQuick.Controls.Material 2.0
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.3
import CustomQmlTypes 1.0

import "../opaque" as Opaque
import "../opaque/controls" as Awesome
import "../utils.js" as Utils
import "../opaque/styles"
import "../opaque/theme"

ColumnLayout {
    Layout.fillWidth: true
    width:parent.width
    id: listpanel
    property bool online: false

    Text {
        Layout.fillWidth: true
    }

    Opaque.IconTextField {
        id: filter
        visible:listpanel.online
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.margins: 10

        //: ex: "Find..."
        placeholderText: qsTr("search-list")
        horizontalAlignment: TextInput.AlignHCenter
        icon: gcd.assetPath + "core/search-24px.webp"


        onTextChanged: {
            listView.filter = text
            if (listView.model.get(listView.currentIndex).title.indexOf(text) == -1) {
                listView.currentIndex = -1
            }
        }
    }



    Opaque.Icon {
        visible:!listpanel.online
        source: gcd.assetPath + "core/negative_heart_24px.webp"
        iconColor: Theme.mainTextColor
        backgroundColor: Theme.backgroundPaneColor
        Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
        height: 150
        width:  150
    }
    Label {
        visible:!listpanel.online
        color: Theme.mainTextColor
        Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
        elide: Text.ElideRight
        font.pixelSize: 18 * gcd.themeScale
        text: qsTr("peer-not-online")
    }




    Flickable { // THE MESSAGE LIST ITSELF
        id: sv
        clip: true
        visible:listpanel.online
        Layout.alignment: Qt.AlignLeft | Qt.AlignTop
        Layout.fillHeight: true
        Layout.fillWidth: true
        contentWidth: parent.width
        contentHeight: parent.height
        boundsBehavior: Flickable.StopAtBounds
        maximumFlickVelocity: 800


		Connections {
			id: cnxns1
			target: mm
			onRowsInserted: {
				var msg = mm.getMessage(first);
				var name = msg.peerID == gcd.selectedProfile ? "me" : mm.getNick(msg.peerID);
				cnxns2.handler(msg.peerID, msg.peerID, name, msg.rawMessage, mm.getImage(msg.peerID), msg.signature, msg.peerID == gcd.selectedProfile, msg.timestamp, msg.ack, msg.error)
			}
		}

        Connections {
        	id: cnxns2
            target: gcd

            onClearMessages: function() {
                jsonModel4.clear()
            }

            onAppendMessage: function(handle, from, displayName, message, image, mid, fromMe, ts, ack, error) {
                handler(handle, from, displayName, message, image, mid, fromMe, ts, ack, error)
            }

            onPrependMessage: function(handle, from, displayName, message, image, mid, fromMe, ts, ack, error) {
                handler(handle, from, displayName, message, image, mid, fromMe, ts, ack, error)
            }


            function handler(handle, from, displayName, message, image, mid, fromMe, ts, ack, error) {
                var msg
                try {
                    msg = JSON.parse(message)
                } catch (e) {
                    return
                }
                if (msg.o != 4) return

                if (msg.t != undefined) {
                    jsonModel4.insert(0,{
                        "title":msg.t,
                        "selected":false,
                        "from": from,
                        "displayName": displayName,
                        "timestamp": ts,
                        "complete": false
                    })
                }

                if(msg.c != undefined) {
                    jsonModel4.get(msg.c).complete = true
                }
            }

            onUpdateContactStatus: function(_handle, _status, _loading) {
                if (gcd.selectedConversation == _handle) {

                    // Group is Synced OR p2p is Authenticated
                    if (  (_handle.length == 32 && _status == 4) || (_handle.length == 56 && _status == 3) ) {
                        newlistitem.readOnly = false
                        listpanel.online = true
                    } else {
                        newlistitem.readOnly = true
                        listpanel.online= false
                    }
                }
            }
        }

        ScrollBar.vertical: ScrollBar{
            policy: ScrollBar.AlwaysOn
        }

        ListView {
            id: listView
            anchors.left: parent.left
            anchors.leftMargin: 10
            anchors.topMargin: 10

            width: parent.width - 50
            height: parent.height - 20
            orientation: Qt.Vertical
            spacing: 10
            model: jsonModel4
            property string filter: ""
            delegate:

            Item {
                width: parent.width
                height:  title.indexOf(listView.filter) >= 0 ? texttitle.height : 0
                visible: title.indexOf(listView.filter) >= 0

                Column {
                    width: parent.width

                    RowLayout {
                        CheckBox {
                            checked: complete
                            onClicked: {
                                var msg = JSON.stringify({"o":4, "c":index})
                                gcd.sendMessage(msg, newlistitem.nextMessageID++)
                            }
                        }

                        RowLayout {
                            Text {
                                id: texttitle
                                text: '<b>' + Utils.htmlEscaped(title) + '</b> by ' + displayName + "<br/>" + Qt.formatDateTime(new Date(timestamp*1000), "MMMM d, h:mm ap")
                                leftPadding: 10
                                topPadding: 5
                                bottomPadding:5
                                color: Theme.mainTextColor
                            }
                        }
                    }

                    Opaque.HLine{}
                }

            }

            focus: true
            ListModel {
                id: jsonModel4
            }
        }
    }

    Opaque.ButtonTextField {
        id: newlistitem
        visible:listpanel.online
        readOnly: false
        button_text: qsTr("add-list-item-btn")
        dropShadowColor: Theme.dropShadowPaneColor
        property int nextMessageID: 1
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.margins: 10

        onClicked: {
            if (newlistitem.text != "") {
                var msg = JSON.stringify({"o":4, "t":newlistitem.text})
                gcd.sendMessage(msg, nextMessageID++)
            }
            newlistitem.text = ""
        }

    }

    Opaque.HLine{}



}
