import QtGraphicalEffects 1.0
import QtQuick 2.7
import QtQuick.Controls 2.4
import QtQuick.Controls.Material 2.0
import QtQuick.Layouts 1.3
import QtQuick.Window 2.11
import QtQuick.Controls 1.4

import "../opaque" as Opaque
import "../opaque/styles"
import "../utils.js" as Utils
import "../opaque/theme"
import "../const"


Rectangle {
    id: root
    color:  Theme.backgroundPaneColor

    function reset() {
        tabBar.currentIndex = 0

        peerAddr.text = ""
        peerName.text = ""

        groupNameCreate.text = ""

        groupAddr.text = ""
        onionLabel.text = gcd.selectedProfile
        gcd.requestPeeredServers()

    }

    Column {
        anchors.fill: parent
        spacing: 25 * gcd.themeScale
        leftPadding: 20 * gcd.themeScale
        rightPadding: 20 * gcd.themeScale

        Opaque.ScalingLabel {
            id: shareLabel
            anchors {
                left: parent.left
                right: parent.right
            }
            horizontalAlignment:Text.AlignHCenter
            size: Theme.secondaryTextSize
            wrapMode: Text.Wrap

            //: Send this address to peers you want to connect with
            text: qsTr("profile-oniblon-label")
        }

        Opaque.ButtonTextField {
            id: onionLabel
            anchors.horizontalCenter: parent.horizontalCenter
            readOnly: true

            width: parent.width - (40*gcd.themeScale)

            button_text: qsTr("copy-btn")
            dropShadowColor: Theme.dropShadowPaneColor
            onClicked: {
                //: notification: copied to clipboard
                gcd.popup(qsTr("copied-to-clipboard-notification"))
                onionLabel.selectAll()
                onionLabel.copy()
            }
        }

        Rectangle { // Spacer
            width: 1
            height: 25 * gcd.themeScale
            color: root.color
        }

        Opaque.TabBar {
            id: tabBar
            visible: gcd.experimentsEnabled && Utils.checkMap(gcd.experiments, "tapir-groups-experiment")
			width: parent.width - parent.leftPadding - parent.rightPadding
			model: [qsTr("add-peer-tab"), qsTr("create-group-tab"), qsTr("join-group-tab")]
		}

        StackLayout {
            id: addStack
            anchors.left: parent.left
            anchors.right: parent.right

            implicitHeight: height
            currentIndex: tabBar.currentIndex

            Column { // Add a peer
                Layout.fillWidth: true
                leftPadding: 20 * gcd.themeScale
                rightPadding: 20 * gcd.themeScale
                spacing: 25 * gcd.themeScale


                Opaque.UnderlineTextField {
                    id: peerAddr

                    backgroundColor: Theme.backgroundPaneColor
                    width: parent.width - (40*gcd.themeScale)
                    anchors.horizontalCenter: parent.horizontalCenter

                    //: Address
                    placeholderText: qsTr("peer-address")

                }

                Opaque.UnderlineTextField {
                    id: peerName

                    backgroundColor: Theme.backgroundPaneColor
                    width: parent.width - (40*gcd.themeScale)
                    anchors.horizontalCenter: parent.horizontalCenter

                    //: Name
                    placeholderText: qsTr("peer-name")

                }
            }

            Column { // Create a group
                Layout.fillWidth: true
                leftPadding: 20 * gcd.themeScale
                rightPadding: 20 * gcd.themeScale
                spacing: 25 * gcd.themeScale


                Opaque.UnderlineTextField {
                    visible: gcd.experimentsEnabled && Utils.checkMap(gcd.experiments, "tapir-groups-experiment")
                    id: groupNameCreate

                    backgroundColor: Theme.backgroundPaneColor
                    width: parent.width - (40*gcd.themeScale)
                    anchors.horizontalCenter: parent.horizontalCenter

                    //: Group Name
                    placeholderText: qsTr("group-name")

                }

                Column {
                    width: parent.width - (40*gcd.themeScale)
                    anchors.horizontalCenter: parent.horizontalCenter
                    visible: gcd.experimentsEnabled && Utils.checkMap(gcd.experiments, "tapir-groups-experiment")
                    Opaque.ScalingLabel {
                        //: Server
                        text: qsTr("server")
                    }
                    Opaque.ComboBox {
                        width:parent.width
                        model: ListModel {
                                id: cbServerList
                        }
                    }
                }
            }

            Column { // Join a group
                Layout.fillWidth: true
                leftPadding: 20 * gcd.themeScale
                rightPadding: 20 * gcd.themeScale
                spacing: 25 * gcd.themeScale


                Opaque.UnderlineTextField {
                    visible: gcd.experimentsEnabled && Utils.checkMap(gcd.experiments, "tapir-groups-experiment")
                    id: groupAddr

                    backgroundColor: Theme.backgroundPaneColor
                    width: parent.width - (40*gcd.themeScale)
                    anchors.horizontalCenter: parent.horizontalCenter

                    //: Address
                    placeholderText: qsTr("group-addr")

                }
            }

        }

        Rectangle { // Spacer
            width: 1
            height: 25 * gcd.themeScale
            color: root.color
        }


        Opaque.Button {
            anchors.horizontalCenter: parent.horizontalCenter

            height: 40 * gcd.themeScale

            //: Add Peer | Create Group | Join Group
            text: tabBar.currentItem.text

            onClicked: {
                if (tabBar.currentIndex == 0) {
                    gcd.addPeer(peerName.text, peerAddr.text)
                    theStack.currentIndex = theStack.emptyPane
                } else if (tabBar.currentIndex == 1) {
                    gcd.createGroup(cbServerList.get(cbServerList.selectedIndex).text, groupNameCreate.text);
                    theStack.currentIndex = theStack.emptyPane
                } else if (tabBar.currentIndex == 2) {
                    gcd.addGroup(groupAddr.text)
                    theStack.currentIndex = theStack.emptyPane
                }
            }
        }

    }

    Connections {
        target: gcd

        onSupplyPeeredServers: function(servers) {
            console.log("Servers are here:!" + servers);
            cbServerList.clear();
            for (var i=0;i<servers.length;i++) {
                cbServerList.append( {"text": servers[i], "value": servers[i]});
            }
        }
    }

}
