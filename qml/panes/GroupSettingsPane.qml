import QtGraphicalEffects 1.0
import QtQuick 2.7
import QtQuick.Controls 2.4
import QtQuick.Controls.Material 2.0
import QtQuick.Layouts 1.3
import QtQuick.Window 2.11
import QtQuick.Controls 1.4

import "../opaque" as Opaque
import "../opaque/styles"
import "../utils.js" as Utils
import "../opaque/theme"
import "../const"

Opaque.SettingsList {  // groupSettingsPane
    id: gsp
    anchors.fill: parent
    property string groupID
    property variant addrbook
    property bool connected: false
    property bool synced: false

    Column {
        anchors.fill: parent

        Opaque.Setting {
            inline: false
            label: qsTr("group-name-label")

            field: Opaque.ButtonTextField {
                id: txtGroupName
                readOnly: false
                button_text: qsTr("save-btn")
                dropShadowColor: Theme.dropShadowPaneColor
                onClicked: {
                    //: notification: copied to clipboard
                    gcd.saveGroupSettings(groupID, txtGroupName.text)
                    theStack.title = txtGroupName.text
                }
            }
        }

        Opaque.Setting {
            inline: false
            label: qsTr("server-label")

            field: Opaque.ButtonTextField {
                id: txtServer
                readOnly: true
                button_text: qsTr("copy-btn")
                dropShadowColor: Theme.dropShadowPaneColor
                onClicked: {
                    //: notification: copied to clipboard
                    gcd.popup(qsTr("copied-to-clipboard-notification"))
                    txtServer.selectAll()
                    txtServer.copy()
                }
            }
        }

        Opaque.Setting {
            inline: false
            label: qsTr("invitation-label")

            field: Opaque.ButtonTextField {
                id: txtInvitation
                readOnly: true
                button_text: qsTr("copy-btn")
                dropShadowColor: Theme.dropShadowPaneColor
                onClicked: {
                    //: notification: copied to clipboard
                    gcd.popup(qsTr("copied-to-clipboard-notification"))
                    txtInvitation.selectAll()
                    txtInvitation.copy()
                }
            }
        }

        Opaque.Setting {
            property color backgroundColor: parent.color
            inline: true
            label: qsTr("server-info")
            field: Column {
                width: parent.width
                spacing:10
                RowLayout {
                    width: parent.width
                    Layout.fillWidth: true
                    Opaque.ScalingLabel {
                        text: gsp.connected ? qsTr("server-connectivity-connected") : qsTr("server-connectivity-disconnected")
                        Layout.alignment: Qt.AlignLeft
                    }
                    Opaque.Icon {
                        backgroundColor: Theme.backgroundPaneColor
                        id: serverStatusIcon
                        height: 18
                        width: 18
                        Layout.alignment: Qt.AlignRight
                        iconColor: gsp.connected ? Theme.statusbarOnlineFontColor : Theme.statusbarDisconnectedTorFontColor
                        source: gcd.assetPath + (gsp.connected ?  "core/signal_cellular_4_bar-24px.webp" : "core/signal_cellular_connected_no_internet_4_bar-24px.webp")
                    }
                }
                RowLayout {
                    width: parent.width
                    Layout.fillWidth: true

                    Opaque.ScalingLabel {
                        text: gsp.synced ? qsTr("server-synced") : qsTr("server-not-synced")
                        Layout.alignment: Qt.AlignLeft
                    }
                    Opaque.Icon {
                        id: serverSyncedStatusIcon
                        backgroundColor: Theme.backgroundPaneColor
                        height: 18
                        width: 18
                        Layout.alignment: Qt.AlignRight
                        iconColor : gsp.synced ? Theme.statusbarOnlineFontColor : Theme.statusbarConnectingFontColor
                        source:  gcd.assetPath + (gsp.synced ? "core/syncing-01.webp" : "core/syncing-03.webp")
                    }
                }

                Opaque.Button {
                    icon: "regular/hdd"
                    text: qsTr("view-server-info")
                    anchors.right: parent.right

                    onClicked: {
                        gcd.requestServerSettings(gcd.selectedConversation)
                        theStack.pane = theStack.serverInfoPane
                    }
                }

            }
        }




        Column {
            width:parent.width * 0.95
            anchors.horizontalCenter: parent.horizontalCenter
            Opaque.Button {
                icon: "regular/trash-alt"
                text: qsTr("delete-btn")
                anchors.right: parent.right


                onClicked: {
                    gcd.leaveGroup(groupID)
                    theStack.pane = theStack.emptyPane
                }
            }
        }

    }

    Connections {
        target: gcd

        onUpdateContactStatus: function(_handle, _status, _loading) {
            if (txtServer.text == _handle) {
                if (_status >= Const.state_connected) {
                    gsp.connected = true
                    serverStatusIcon
                    if (_status != Const.state_synced) {
                        gsp.synced = false
                    } else {
                        gsp.synced = true
                    }
                } else {
                    gsp.connected = false
                    gsp.synced = false
                }
            }
        }

        onSupplyGroupSettings: function(gid, name, server, invite, accepted, addrbooknames, addrbookaddrs) {
            gsp.groupID = gid
            txtGroupName.text = name
            txtServer.text = server
            txtInvitation.text = invite
            //cbInvite.model = addrbooknames.map(function(e){return Utils.htmlEscaped(e)})
            addrbook = addrbookaddrs
        }
    }
}
