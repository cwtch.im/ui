import QtGraphicalEffects 1.0
import QtQuick 2.7
import QtQuick.Controls 2.4
import QtQuick.Controls.Material 2.0
import QtQuick.Layouts 1.3

import "../opaque" as Opaque
import "../opaque/styles"
import "../overlays"
import "../opaque/fonts"
import "../opaque/theme"


ColumnLayout {
    Layout.fillWidth: true
    id: overlay

    property string name
    property bool accepted
    property bool inGroup

    Opaque.TabBar {
        id: tabBar
        Layout.fillWidth: true

		model: [qsTr("chat-btn"), qsTr("lists-btn"), qsTr("bulletins-btn")]
		onCurrentIndexChanged: overlayStack.overlay = currentIndex
	}

    StackLayout {
        id: overlayStack
        Layout.fillWidth: true
        width: parent.width


        implicitHeight: height
        currentIndex: 0

        property alias overlay: overlayStack.currentIndex
        readonly property int chatOverlay: 0
        readonly property int listOverlay: 1
        readonly property int bulletinOverlay: 2
        readonly property int membershipOverlay: 3


        ChatOverlay { //0
            Layout.maximumHeight: overlayStack.height
            Layout.maximumWidth: overlayStack.width
        }

        ListOverlay{ //1
            Layout.maximumHeight: overlayStack.height
            Layout.maximumWidth: overlayStack.width
        }

        BulletinOverlay{ //2
            Layout.maximumHeight: overlayStack.height
            Layout.maximumWidth: overlayStack.width
        }

        MembershipOverlay { //3
            Layout.maximumHeight: overlayStack.height
            Layout.maximumWidth: overlayStack.width
        }
    }

    Connections {
        target: gcd

        onResetMessagePane: function() {
            tabBar.currentIndex = 0
            overlay.inGroup = false
        }

        onSupplyGroupSettings: function(gid, name, server, invite, accepted, addrbooknames, addrbookaddrs) {
            overlay.name = name
            overlay.accepted = accepted
            overlay.inGroup = true
        }

        onSupplyServerSettings: function(server) {
            overlay.name = server
        }


    }
}
