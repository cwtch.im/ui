import QtGraphicalEffects 1.0
import QtQuick 2.7
import QtQuick.Controls 2.4
import QtQuick.Controls.Material 2.0
import QtQuick.Layouts 1.3
import QtQuick.Window 2.11
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4

import "../opaque" as Opaque
import "../opaque/styles"
import "../opaque/theme"
import "../const"

Opaque.SettingsList { // settingsPane
    id: root
    anchors.fill: parent
    anchors.topMargin: 20
    width: parent.width
    height: parent.height
    contentHeight: peerSettings.height + 20
    property string authorization
    property string saveHistory

    Column {
        id: peerSettings
        anchors.horizontalCenter: parent.horizontalCenter
        width:parent.width -20
        parent: root.contentItem

        Opaque.Setting {
            inline: false
            label: qsTr("address-label")

            field: Opaque.ButtonTextField {
                id: txtOnion
                readOnly: true
                button_text: qsTr("copy-btn")
                dropShadowColor: Theme.dropShadowPaneColor
                onClicked: {
                    //: notification: copied to clipboard
                    gcd.popup(qsTr("copied-to-clipboard-notification"))
                    txtOnion.selectAll()
                    txtOnion.copy()
                }
            }
        }



        Opaque.Setting {
            inline: false

            label: qsTr("display-name-label")

            field: Opaque.ButtonTextField {
                id: txtDisplayName
                button_text: qsTr("save-btn")
                dropShadowColor: Theme.dropShadowPaneColor
                onClicked: {
                    gcd.savePeerSettings(txtOnion.text, txtDisplayName.text)
                    toolbar.setTitle(txtDisplayName.text)

                    // FIXME this is kind of a hack as ideally we could just update the peer name
                    // and have that change broadcast to each message - but there isn't an easy way to do that
                    // with our current message model setup. As such we simply reset and reload the message pane
                    gcd.broadcast("ResetMessagePane")
                    theStack.pane = theStack.messagePane
                    mm.setHandle(txtOnion.text)
                    gcd.loadMessagesPane(txtOnion.text)
                }
            }
        }


        Opaque.Setting {
            label: qsTr("block-btn")


            field: Opaque.ToggleSwitch {
                anchors.right: parent.right

                isToggled: root.authorization == Const.auth_blocked
                onToggled: function() {
                    if (root.authorization == Const.auth_blocked) {
                        root.authorization = Const.auth_unknown
                        gcd.setPeerAuthorization(txtOnion.text, Const.auth_unknown)
                    } else {
                        root.authorization = Const.auth_blocked
                        gcd.setPeerAuthorization(txtOnion.text, Const.auth_blocked)
                    }
                    isToggled = root.authorization == Const.auth_blocked
                }
            }
        }

        Opaque.Setting {
            //: Save Peer History
            label: qsTr("save-peer-history")
            description: qsTr("save-peer-history-description")
            field: Opaque.ComboBox {
                id: cbSaveHistory
                anchors.right: parent.right
                anchors.left: parent.left

                model: ListModel {
                    id: cbSaveHistoryItems
                    ListElement { text: qsTr("dont-save-peer-history"); value: "DeleteHistoryConfirmed" }
                    ListElement { text: qsTr("save-peer-history"); value: "SaveHistory" }
                }

                onActivated: {
                    var item = cbSaveHistoryItems.get(cbSaveHistory.currentIndex)
                    if (item["value"] == "SaveHistory") {
                        gcd.storeHistoryForPeer(txtOnion.text)
                    } else {
                        gcd.deleteHistoryForPeer(txtOnion.text)
                    }
                }

            }
        }


        Column {
            width:parent.width * 0.95
            anchors.horizontalCenter: parent.horizontalCenter
            Opaque.Button {
                icon: "regular/trash-alt"
                text: qsTr("delete-btn")
                anchors.right: parent.right


                onClicked: {
                    gcd.deleteContact(txtOnion.text)
                    theStack.pane = theStack.emptyPane
                }
            }
        }

    }


    Connections {
        target: gcd

        onSupplyPeerSettings: function(onion, nick, authorization, saveHistory) {
            txtOnion.text = onion
            txtDisplayName.text = nick
            root.authorization = authorization
            root.saveHistory = saveHistory

            // This will not set the value in the default case where saveHistory = DefaultDeleteHistory and thus
            // the combobox will default to showing DeleteHistoryConfirmed
            for (var i=0; i < cbSaveHistoryItems.count; i++) {
                var item = cbSaveHistoryItems.get(i)
                if (item["value"] == root.saveHistory || (root.saveHistory == "DefaultDeleteHistory" && item["value"] == "DeleteHistoryConfirmed")) {
                    cbSaveHistory.currentIndex = i
                    break
                }
            }
        }
    }


}
