import QtQuick 2.0
import QtGraphicalEffects 1.0
import QtQuick 2.7
import QtQuick.Controls 2.4
import QtQuick.Controls.Material 2.0
import QtQuick.Layouts 1.3
import QtQuick.Window 2.11
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4

import "../widgets"

import "../opaque" as Opaque
import "../opaque/controls"
import "../opaque/styles"
import "../opaque/theme"
import "../opaque/fonts"

import "../utils.js" as Utils

ColumnLayout {
    id: thecol
    anchors.fill: parent

    spacing: 20 * gcd.themeScale

    Opaque.ScalingLabel {
        Layout.alignment: Qt.AlignHCenter
        wrapMode: TextEdit.Wrap
        size: Theme.primaryTextSize

        font.family: Fonts.applicationFontRegular.name
        font.styleName: "Bold"

        //: Enter a password to view your profiles
        text: qsTr("enter-profile-password")
    }

    RowLayout {
        width: 700 * gcd.themeScale
        Layout.alignment: Qt.AlignHCenter

        Opaque.UnderlineTextField {
            id: txtPassword
            //anchors.horizontalCenter: parent.horizontalCenter
            //Layout.fillWidth: true
            Layout.maximumWidth: 450 * gcd.themeScale
            Layout.minimumWidth: 450 * gcd.themeScale

            Layout.rightMargin: 40
            Layout.leftMargin: 40
            placeholderText: qsTr("password")
            echoMode: TextInput.Password
            onAccepted: button.clicked()
        }

        Opaque.Button {
            id: "button"
            width: 100 * gcd.themeScale
            height: Theme.primaryTextSize * gcd.themeScale

            //: Unlock
            text: qsTr("unlock")

            onClicked: {
                gcd.unlockProfiles(txtPassword.text)
                txtPassword.text = ""
            }
        }
    }

    Opaque.ScalingLabel {
        id: error
        Layout.alignment: Qt.AlignHCenter
        color: Theme.textfieldErrorColor

        visible: false
    }



    Connections { // ADD/REMOVE CONTACT ENTRIES
        target: gcd

        onErrorLoaded0: function() {
            error.visible = true
        }
    }

    // space
    Rectangle {
        width: 1
        height: 20 * gcd.themeScale
        color: Theme.backgroundMainColor
    }

    Opaque.ResponsiveContainer {
        Layout.fillHeight: true
        Layout.fillWidth: true

        Rectangle {
            color: Theme.backgroundMainColor

            Layout.fillHeight: true
            Layout.fillWidth: true

            Opaque.ScalingLabel {
                id: profileLabel
                size: Theme.subHeaderSize
                font.family: Fonts.applicationFontRegular.name
                font.styleName: "Bold"
                anchors.left: parent.left
                anchors.leftMargin: 25 * gcd.themeScale
                bottomPadding: 10 * gcd.themeScale

                //: Your Profiles
                text: qsTr("your-profiles")
            }

            ProfileList {
                anchors.top: profileLabel.bottom
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.bottom: parent.bottom
            }
        }

        Rectangle {
            // TODO Remove Experiment Check once Feature is Stable
            visible: gcd.experimentsEnabled && Utils.checkMap(gcd.experiments, "tapir-servers-experiment")
            color: Theme.backgroundMainColor

            Layout.fillHeight: true
            Layout.fillWidth: true

            Opaque.ScalingLabel {
                id: serverLabel
                size: Theme.subHeaderSize
                font.family: Fonts.applicationFontRegular.name
                font.styleName: "Bold"
                anchors.left: parent.left
                anchors.leftMargin: 25 * gcd.themeScale
                bottomPadding: 10 * gcd.themeScale

                //: Your Profiles
                text: qsTr("your-servers")
            }

            ServerList {
                anchors.top: serverLabel.bottom
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.bottom: parent.bottom
            }
        }

    }



}
