import QtGraphicalEffects 1.0
import QtQuick 2.7
import QtQuick.Controls 2.13
import QtQuick.Controls.Material 2.0
import QtQuick.Layouts 1.3
import QtQuick.Window 2.11


import "../opaque" as Opaque
import "../opaque/theme"

Opaque.SettingsList { // Add Profile Pane
    id: serverAddEditPane
    anchors.fill: parent

    function reset() {
        serverAddEditPane.server_name = "";
        serverAddEditPane.server_available = false;
        serverAddEditPane.server_bundle = ""
    }

    property string server_name;
    property bool server_available;
    property string server_bundle;
    property bool autostart_server;
    property int server_messages;

    function load(server_onion, server_name, server_available, autostart_server, server_messages, server_bundle) {
        reset();
        serverAddEditPane.server_name = server_name;
        serverAddEditPane.server_available = server_available;
        serverAddEditPane.server_bundle = server_bundle;
        serverAddEditPane.autostart_server = autostart_server;
        serverAddEditPane.server_messages = server_messages;
    }


    Column {
        anchors.horizontalCenter: parent.horizontalCenter
        width: 700

        Opaque.ScalingLabel {
            text: server_name
            size: 16
        }

        Opaque.Setting {
            label: qsTr("server-availability")


            field: Opaque.ToggleSwitch {
                anchors.right: parent.right

                isToggled: serverAddEditPane.server_available
                onToggled: function() {
                    serverAddEditPane.server_available = !serverAddEditPane.server_available
                    if (serverAddEditPane.server_available) {
                        gcd.startServer(serverAddEditPane.server_name)
                    } else {
                        gcd.stopServer(serverAddEditPane.server_name)
                    }
                }
            }
        }







        Opaque.Setting {
            label: qsTr("server-autostart")

            field: Opaque.ToggleSwitch {
                anchors.right: parent.right

                isToggled: serverAddEditPane.autostart_server
                onToggled: function() {
                    serverAddEditPane.autostart_server = !serverAddEditPane.autostart_server
                    gcd.autostartServer(serverAddEditPane.server_name, serverAddEditPane.autostart_server)
                }
            }
        }

        Opaque.Setting {
            inline: false
            label: qsTr("server-num-messages")

            field: Opaque.TextField {
                id: numMessages
                readOnly: true
                text: serverAddEditPane.server_messages;
            }
        }


        Opaque.Setting {
            inline: false
            label: qsTr("server-key-bundle")

            field: Opaque.ButtonTextField {
                id: txtServerBundle
                readOnly: true
                text: serverAddEditPane.server_bundle;
                button_text: qsTr("copy-btn")
                dropShadowColor: Theme.dropShadowPaneColor
                onClicked: {
                    //: notification: copied to clipboard
                    gcd.popup(qsTr("copied-to-clipboard-notification"))
                    txtServerBundle.selectAll()
                    txtServerBundle.copy()
                }
            }
        }

    }


    Connections {
        target: gcd
    }
}
