import QtGraphicalEffects 1.0
import QtQuick 2.7
import QtQuick.Controls 2.4
import QtQuick.Controls.Material 2.0
import QtQuick.Layouts 1.3
import QtQuick.Window 2.11
import QtQuick.Controls 1.4

import "../opaque" as Opaque
import "../opaque/styles"
import "../utils.js" as Utils
import "../opaque/theme"
import "../const"

Opaque.SettingsList {  // groupSettingsPane
    id: gsp
    anchors.fill: parent
    property string serverName
    property color backgroundColor: parent.color
    property bool connected: false
    property bool synced: false

    Column {
        anchors.fill: parent

        Opaque.Setting {
            inline: false
            label: qsTr("server-label")

            field: Opaque.ButtonTextField {
                id: txtServer
                readOnly: true
                text: serverName;
                button_text: qsTr("copy-btn")
                dropShadowColor: Theme.dropShadowPaneColor
                onClicked: {
                    //: notification: copied to clipboard
                    gcd.popup(qsTr("copied-to-clipboard-notification"))
                    txtServer.selectAll()
                    txtServer.copy()
                }
            }
        }
    }

    Connections {
        target: gcd

        onUpdateContactStatus: function(_handle, _status, _loading) {
            if (txtServer.text == _handle) {
                if (_status >= Const.state_connected) {
                    gsp.connected = true
                    if (_status != Const.state_synced) {
                        gsp.synced = false
                    } else {
                        gsp.synced = true
                    }
                } else {
                    gsp.connected = false
                    gsp.synced = false
                }
            }
        }

        onSupplyServerSettings: function(server, key_names, keys) {
            gsp.serverName = server;
            toolbar.setTitle(qsTr("server-settings"));
            console.log("Servers: " + key_names);
            for (let i=0; i<key_names.length;i++) {
                // TODO: Actually do something with these keys
                console.log("FOUND SERVER KEY " + key_names[i] + " " + keys[i]);
            }
        }
    }

}
