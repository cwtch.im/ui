import QtGraphicalEffects 1.0
import QtQuick 2.7
import QtQuick.Controls 2.4
import QtQuick.Controls.Material 2.0
import QtQuick.Layouts 1.3
import QtQuick.Window 2.11
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4

import "../opaque" as Opaque
import "../opaque/controls"
import "../opaque/theme"
import "../widgets" as Widgets
import "../utils.js" as Utils

Opaque.SettingsList { // settingsPane
    id: root
    anchors.fill: parent
    width: parent.width
    height: parent.height
    anchors.topMargin: 20
    contentHeight: settings.height

    Column {
        id: settings
        anchors.horizontalCenter: parent.horizontalCenter
        width:parent.width -20
        parent:root.contentItem

        Opaque.Setting {

            //: Language
            label: qsTr("setting-language")

            field: Opaque.ComboBox {
                id: cbLanguage
                anchors.right: parent.right
                anchors.left: parent.left

                property bool inited: false

                model: ListModel {
                    id: cbLangItems
                    ListElement { text: qsTr("locale-en"); value: "en" }
                    ListElement { text: qsTr("locale-fr"); value: "fr" }
                    ListElement { text: qsTr("locale-es"); value: "es" }
                    ListElement { text: qsTr("locale-de"); value: "de" }
                    ListElement { text: qsTr("locale-it"); value: "it" }
                    ListElement { text: qsTr("locale-pt"); value: "pt" }
                }

                onCurrentIndexChanged: {
                    var item = cbLangItems.get(cbLanguage.currentIndex)
                    // Comboboxes seem to fire one Change on load...
                    if (!cbLanguage.inited) {
                        cbLanguage.inited = true
                        return
                    }
                    gcd.locale = item["value"]
                }

            }
        }

        Opaque.Setting {
            //: Interface Zoom
            label: qsTr("setting-interface-zoom")

            field: Row {
                spacing: 10
                anchors.verticalCenter: parent.verticalCenter

                Opaque.ScalingLabel {
                    text: qsTr("small-text-label")
                    size: 8
                }

                Opaque.Slider {
                    id: zoomSlider
                    from: 0.5
                    to: 4.0
                    value: gcd.themeScale
                    live: false
                    snapMode: Slider.SnapAlways
                    stepSize: 0.25

                    onValueChanged: {
                        gcd.themeScale = zoomSlider.value
                    }
                    width: 200
                }


                Opaque.ScalingLabel {
                    anchors.verticalCenter: parent.verticalCenter
                    wrapMode: TextEdit.Wrap
                    text: qsTr("large-text-label")
                    size: 20
                }
            }
        }

        Opaque.Setting {
            //: Theme
            label: qsTr("setting-theme")

            field: Opaque.ComboBox {
                id: cbTheme
                property bool inited: false
                anchors.right: parent.right
                anchors.left: parent.left
                model: ListModel {
                    id: cbThemeItems
                    ListElement { text: qsTr("theme-light"); value: 'light' }
                    ListElement { text: qsTr("theme-dark"); value: 'dark' }
                }

                onCurrentIndexChanged: {
                    var item = cbThemeItems.get(cbTheme.currentIndex)
                    // Comboboxes seem to fire one Change on load...
                    if (!cbTheme.inited) {
                        cbTheme.inited = true
                        return
                    }
                    gcd.theme = item["value"]
                }
            }
        }

        // Experimental Gating

        Opaque.Setting {
            //: Theme
            label: qsTr("experiments-enabled")

            field: Opaque.ToggleSwitch {
                anchors.right: parent.right
                id: experimentsEnabledToggle
                isToggled: gcd.experimentsEnabled
                onToggled: function() {
                    console.log("experiments enabled: " +  gcd.experimentsEnabled + " " + experimentsEnabledToggle.isToggled) ;
                    if  (gcd.experimentsEnabled == false) {
                        gcd.experimentsEnabled = true;
                    } else {
                        gcd.experimentsEnabled = false;
                    }
                }
            }
        }

        Widgets.ExperimentToggle {
            name: "servers_enabled"
            experiment_id: "tapir-servers-experiment"
        }

        Widgets.ExperimentToggle {
            name: "groups_enabled"
            experiment_id: "tapir-groups-experiment"
        }
            Opaque.ScalingLabel {
                id: versionLabel
                anchors.horizontalCenter: parent.horizontalCenter
                //: Version %1 with tor %2
                text: qsTr("version %1 tor %2").arg(gcd.version).arg(gcd.torVersion)
            }

            Opaque.ScalingLabel {
                id: builddateLabel
                anchors.horizontalCenter: parent.horizontalCenter
                //: Built on: %2
                text: qsTr("builddate %2").arg(gcd.buildDate)
            }

    }


    Connections {
        target: gcd

        onSupplySettings: function(locale, zoom, theme) {
            for (var i=0; i < cbLangItems.count; i++) {
                var item = cbLangItems.get(i)
                if (item["value"] == locale) {
                    cbLanguage.currentIndex = i
                    break
                }
            }

            for (var i=0; i < cbThemeItems.count; i++) {
                var item = cbThemeItems.get(i)
                if (item["value"] == theme) {
                    cbTheme.currentIndex = i
                    break
                }
            }
        }
    }






    //end of flickable
}

