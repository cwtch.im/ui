import QtQuick 2.7
import QtQuick.Controls 2.4
import QtQuick.Controls.Material 2.0
import QtQuick.Layouts 1.3
import QtQuick.Window 2.11
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
import "../opaque/styles"
import "../opaque" as Opaque
import "../opaque/theme"

Item {
    id: sp
    property bool running


    AnimatedImage {
        id: aniKnot
        playing: sp.running
        source: "qrc:/qml/images/knot_animated.gif"

        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: parent.top
        anchors.topMargin: 100 * gcd.themeScale

        width: 200
        height: 200
    }

    Image {
        id: splashImage
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: aniKnot.bottom
        anchors.topMargin: 20 * gcd.themeScale

        source: "qrc:/qml/images/cwtch_title.png"
    }


    Opaque.ScalingLabel {
        id: loadingLabel
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: splashImage.bottom
        anchors.topMargin: 10 * gcd.themeScale

        //: Loading tor...
        text: qsTr("loading-tor")

        size: Theme.secondaryTextSize
        color: Theme.mainTextColor
    }

    Image {
        id: opImage
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 20 * gcd.themeScale

        source: "qrc:/qml/images/Open_Privacy_Logo_lightoutline.png"
    }
}
