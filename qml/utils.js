.pragma library

function htmlEscaped(str) {
    str = str.replace(/</g, "&lt;");
    str = str.replace(/>/g, "&gt;");
    return str
}


function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min)) + min; //The maximum is exclusive and the minimum is inclusive
}

function mouseToGrid(x,y) {
    return {"x":Math.floor(x/40), "y":Math.floor(y/40)}
}

function distance(a,b) {
    let ax = Math.floor(a/16)
    let ay = a%16

    let bx = Math.floor(b/16)
    let by = b%16

    return Math.abs(ax-bx) + Math.abs(ay-by)

}

function scorePath(path) {
    var pathLength = 0
    for (var i=0;i<path.length-1;i++) {
        pathLength += distance(path[i],path[i+1])
    }
    return ""+pathLength
}

function isGridOccupied(x, y, points) {
    var inPoints = false
    for (var i=0;i<points.length;i++) {
        if (((x*16)+y) == points[i]) {
            inPoints = true
        }
    }
    return inPoints
}

function checkMap(obj, key) {
    let map = buildMap(obj);
    if (map[key] != undefined) {
        return map[key];
    }
    return false
}

function buildMap(obj) {
    let map = new Map();
    Object.keys(obj).forEach(key => {
        map[key] =  obj[key];
    });
    return map;
}

function isGroup(id) {
    return id.length == 32
}

function isPeer(id) {
    return id.length == 56
}