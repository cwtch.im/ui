import QtGraphicalEffects 1.0
import QtQuick 2.7
import QtQuick.Controls 2.4
import QtQuick.Controls.Material 2.0
import QtQuick.Layouts 1.3
import CustomQmlTypes 1.0
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4

import "../opaque" as Opaque
import "../opaque/styles"
import "../opaque/theme"
import "../const"
import "../utils.js" as Utils

Opaque.PortraitRow {
    property int status: Const.state_disconnected
    property int badge
    property bool loading
    property string authorization

    property bool blocked

    portraitOverlayColor: Theme.portraitOverlayOfflineColor

    badgeColor: Theme.portraitOnlineBadgeColor
    badgeVisible: (Utils.isGroup(handle) && status == Const.state_synced) || (Utils.isPeer(handle) && status == Const.state_authenticated)

    ProgressBar { // LOADING ?
        id: loadingProgress
        property bool running
        running: loading
        visible: loading

        anchors.right: parent.right
        anchors.verticalCenter: parent.verticalCenter
        anchors.leftMargin: 1 * gcd.themeScale
        anchors.rightMargin: 25 * gcd.themeScale

        height: parent.height * .1
        width: 100 * gcd.themeScale

        indeterminate: true

        style: ProgressBarStyle {
            progress: CwtchProgress { running: loadingProgress.running}
        }
    }

    Opaque.Badge {
        id: unreadBadge
        visible: badge > 0
        color: Theme.portraitContactBadgeColor
        size: parent.height/4

        anchors.right: parent.right
        anchors.rightMargin: 25 * gcd.themeScale
        anchors.leftMargin: 1 * gcd.themeScale
        anchors.verticalCenter: parent.verticalCenter

        content: Label {
            id: lblUnread
            color: Theme.portraitContactBadgeTextColor
            font.pixelSize: Theme.badgeTextSize * gcd.themeScale
            font.weight: Font.Bold
            text: badge > 99 ? "99+" : badge
        }
    }

    Column {
        visible: authorization == Const.auth_unknown
        anchors.right: parent.right
        anchors.verticalCenter: parent.verticalCenter
        anchors.leftMargin: 1 * gcd.themeScale
        anchors.rightMargin: 25 * gcd.themeScale
        spacing: 16 * gcd.themeScale

        Opaque.Icon {
            source: gcd.assetPath + "core/favorite-24px.webp"
            iconColor: Theme.toolbarIconColor
            backgroundColor: rowColor
            height: 18 * gcd.themeScale
            width:  18 * gcd.themeScale

            onClicked: { gcd.setPeerAuthorization(handle, Const.auth_approved)}
        }

        Opaque.Icon {
            source: gcd.assetPath + "core/delete-24px.webp"
            iconColor: Theme.toolbarIconColor
            backgroundColor: rowColor
            height: 18 * gcd.themeScale
            width:  18 * gcd.themeScale

            onClicked: { console.log("approve"); gcd.setPeerAuthorization(handle, Const.auth_blocked)}
        }
    }

    onClicked: function() {
        gcd.broadcast("ResetMessagePane")
        theStack.pane = theStack.messagePane
        mm.setHandle(handle)
        gcd.loadMessagesPane(handle)
        badge = 0
    }

    Component.onCompleted: { setColors(status) }

    onStatusChanged: { setColors(status) }

    function setColors(status) {
        //-2:WtfCodeError,-1:Error,0:Disconnected,1:Connecting,2:Connected,3:Authenticated,4:Synced,5:Failed,6:Killed

        if (authorization == Const.auth_blocked) {
            portraitBorderColor = Theme.portraitBlockedBorderColor
            portraitColor = Theme.portraitBlockedBackgroundColor
            nameColor = Theme.portraitBlockedTextColor
            onionColor = Theme.portraitBlockedTextColor
            portraitPerformTransform = true
        } else if (status == Const.state_synced || status == Const.state_authenticated) {
            portraitBorderColor = Theme.portraitOnlineBorderColor
            portraitColor = Theme.portraitOnlineBackgroundColor
            nameColor = Theme.portraitOnlineTextColor
            onionColor = Theme.portraitOnlineTextColor
            portraitPerformTransform = false
        } else if (status == Const.state_connected  || status == Const.state_connecting) {
            portraitBorderColor = Theme.portraitConnectingBorderColor
            portraitColor = Theme.portraitConnectingBackgroundColor
            nameColor = Theme.portraitConnectingTextColor
            onionColor = Theme.portraitConnectingTextColor
            portraitPerformTransform = true
        } else {
            portraitBorderColor = Theme.portraitOfflineBorderColor
            portraitColor = Theme.portraitOfflineBackgroundColor
            nameColor = Theme.portraitOfflineTextColor
            onionColor = Theme.portraitOfflineTextColor
            portraitPerformTransform = true
        }
    }

    Connections { // UPDATE UNREAD MESSAGES COUNTER
        target: gcd

        onUpdateContactStatus: function(_handle, _status, _loading) {
            if (handle == _handle) {
                status = _status
                loadingProgress.visible = loadingProgress.running = loading = _loading
            }
        }

        onIncContactMessageCount: function(_profile, _handle) {
            if (_profile == gcd.selectedProfile && handle == _handle && gcd.selectedConversation != handle) {
                badge++
            }
        }

        onSelectedConversationChanged: function() {
            if (handle == gcd.selectedConversation) {
                isActive = true
                badge = 0
            } else {
                isActive = false
            }
        }
    }
}
