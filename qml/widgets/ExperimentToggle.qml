import "../opaque" as Opaque
import "../opaque/controls"
import "../opaque/theme"
import "../utils.js" as Utils

Opaque.Setting {
    //: Theme
    id: experiment
    property string name;
    property string experiment_id;
    visible: gcd.experimentsEnabled
    label: qsTr(name)

    field: Opaque.ToggleSwitch {
        anchors.right: parent.right
        id: expToggle
        isToggled: Utils.checkMap(gcd.experiments, experiment.experiment_id)
        onToggled: function() {
            let experimentsMap =  Utils.buildMap(gcd.experiments);
            experimentsMap[experiment.experiment_id] =  expToggle.isToggled ? false : true;
            gcd.experiments = experimentsMap;
        }
    }
}
