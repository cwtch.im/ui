import QtGraphicalEffects 1.0
import QtQuick 2.7
import QtQuick.Controls 2.4
import QtQuick.Controls.Material 2.0
import QtQuick.Controls.Styles 1.4
import QtQuick.Layouts 1.3
import QtQuick.Window 2.11
import QtQuick.Controls 1.4

import "../opaque" as Opaque
import "../opaque/styles"
import "../opaque/theme"
import "../opaque/fonts"
import "../const"

Item {
    id: root
    anchors.fill: parent
    width: parent.width

    height: profile.height
    implicitHeight: profile.height

    property string image
    property string nick
    property string onion
    property string tag
    property bool dualPane: false
    property bool profileOnline: false


    property real logscale: 4 * Math.log10(gcd.themeScale + 1)

    onDualPaneChanged: { realignProfile() }

    function realignProfile() {

        if (dualPane) {
            profile.height = Theme.contactPortraitSize * logscale

            portrait.anchors.horizontalCenter = undefined
            portrait.anchors.left = profile.left
            portrait.anchors.leftMargin = 25 * logscale

            portrait.size = Theme.contactPortraitSize * logscale

            profiletype.height = Theme.badgeTextSize * gcd.themeScale

            nameRow.anchors.right = undefined
            nameRow.anchors.left = portrait.right

            nameRow.anchors.top = undefined
            nameRow.anchors.verticalCenter = portrait.verticalCenter

            nameCenter.anchors.horizontalCenter = undefined
            nameCenter.anchors.left = nameRow.left
        } else {
            profile.height = (Theme.contactPortraitSize * 2 * logscale)

            portrait.anchors.left =  undefined
            portrait.anchors.leftMargin = undefined
            portrait.anchors.horizontalCenter = profile.horizontalCenter

            portrait.size = Theme.contactPortraitSize * 1.5

            profiletype.height = Theme.badgeTextSize * gcd.themeScale * 1.5

            nameRow.anchors.left =  profile.left
            nameRow.anchors.right = profile.right

            nameRow.anchors.verticalCenter = undefined
            nameRow.anchors.top = portrait.bottom

            nameCenter.anchors.left = undefined
            nameCenter.anchors.horizontalCenter = nameRow.horizontalCenter

        }
    }

    Rectangle {

        anchors.left: parent.left
        anchors.right: parent.right
        width: parent.width
        id: profile
        color: Theme.backgroundMainColor

        Opaque.Portrait {
            id: portrait

            source: root.image

            badgeColor: Theme.portraitProfileBadgeColor
            portraitBorderColor: Theme.portraitOnlineBorderColor
            portraitColor: Theme.portraitOnlineBackgroundColor

            badgeContent: Opaque.Icon {// Profle Type
                id: profiletype
                source: tag == "v1-userPassword" ? gcd.assetPath + "core/lock-24px.webp" : gcd.assetPath + "core/lock_open-24px.webp"
                height: Theme.badgeTextSize * gcd.themeScale
                width: height

                iconColor: Theme.defaultButtonTextColor
                backgroundColor: portrait.badgeColor
                hilightBackgroundColor: portrait.badgeColor
            }
        }

        Rectangle {
            id: nameRow
            height: name.height
            color: Theme.backgroundMainColor

            Rectangle {
                id: nameCenter
                width: name.width + addBtn.width

                Label {
                    id: name

                    color: Theme.portraitOnlineTextColor
                    elide: Text.ElideRight
                    font.pixelSize: Theme.usernameSize * gcd.themeScale
                    font.weight: Font.Bold
                    font.family: Fonts.applicationFontExtraBold.name
                    font.styleName: "ExtraBold"
                    text: nick
                }

                Opaque.Button { // Add Button
                    id: addBtn

                    anchors.left: name.right
                    anchors.top: name.top
                    anchors.leftMargin: 10 * gcd.themeScale

                    icon: "fontawesome/solid/plus"

                    height: name.height
                    width: height
                    radius: width * 0.3
                    onClicked: {
                        theStack.currentIndex = theStack.addPeerGroupPane
                    }
                }
            }
        }

    }

    function updateStatus() {
        if (gcd.torStatus != Const.statusOnline) { // Tor network offline
            portrait.portraitBorderColor = Theme.portraitOfflineBorderColor
            portrait.portraitColor = Theme.portraitOfflineBackgroundColor
            name.color = Theme.portraitOfflineTextColor
        } else {
            // TODO: update to include logic on if a peer wants to be online or not (not implemented)

            if (profileOnline == false) {
                portrait.portraitBorderColor = Theme.portraitConnectingBorderColor
                portrait.portraitColor = Theme.portraitConnectingBackgroundColor
                name.color = Theme.portraitConnectingTextColor
            } else {
                portrait.portraitBorderColor = Theme.portraitOnlineBorderColor
                portrait.portraitColor = Theme.portraitOnlineBackgroundColor
                name.color = Theme.portraitOnlineTextColor
            }
        }
    }

    Component.onCompleted: { updateStatus() }

    Connections {
        target: gcd

        onUpdateMyProfile: function(_nick, _onion, _image, _tag, _showBlocked, _online) {
            nick = _nick
            onion = _onion
            image = _image
            tag = _tag
            profileOnline = _online
            updateStatus()
        }

        onResetProfile: { realignProfile() }

        onTorStatusChanged: function() {
            updateStatus()
        }

        onUpdateProfileNetworkStatus: function(_onion, online) {
            if (onion == _onion) {
                profileOnline = online
                updateStatus()
            }
        }

    }

    Connections {
        target: Theme

        onThemeChanged: {
            updateStatus()
        }
    }
}
