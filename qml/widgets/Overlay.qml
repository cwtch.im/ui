import QtGraphicalEffects 1.0
import QtQuick 2.7
import QtQuick.Controls 2.4
import QtQuick.Controls.Material 2.0
import QtQuick.Layouts 1.3

import "../opaque" as Opaque
import "../opaque/controls" as Awesome
import "../utils.js" as Utils
import "../widgets"
import "../opaque/theme"
import "../const"


Item {
    id: root
    Layout.fillWidth: true
    property bool online: false

    property int state: Const.state_disconnected
    property string authorization: ""


    signal sendClicked(string messateText)
    property alias contentItem: control.contentItem

    Control {
        id: control
        width: parent.width
        anchors.top: parent.top
        anchors.topMargin: 10

        anchors.bottom: msgEd.top
        anchors.bottomMargin: 10

        horizontalPadding: 15

    }


    MessageEditor {
        id: msgEd

        anchors.bottom: parent.bottom
        anchors.right: parent.right
        anchors.left: parent.left
        anchors.rightMargin: 15
        anchors.leftMargin: 15

        onSendClicked: function(messageText) {
            root.sendClicked(messageText)
        }
    }

    function updateState() {
        if (root.authorization == Const.auth_blocked) {
            // Blocked

        } else if ( (Utils.isGroup(gcd.selectedConversation) && root.state == Const.state_synced) || (Utils.isPeer(gcd.selectedConversation) && root.state == Const.state_authenticated) ) {
            // Online

        } else {
            // Not Online

        }
    }

    Connections {
        target: gcd

        onUpdateContactStatus: function(_handle, _status, _loading) {
            if (gcd.selectedConversation == _handle) {
                root.state = _status
                msgEd.state = _status
                updateState()
                msgEd.updateState()
            }
        }

        onSupplyPeerSettings: function(onion, nick, authorization, storage) {
            // TODO unknown
            root.authorization = authorization
            msgEd.authorization = authorization
            updateState()
            msgEd.updateState()
        }

        onSupplyGroupSettings: function(groupID, nick, groupServer, invite, accepted, contactnames, contactaddrs) {
            if (accepted) {
                root.authorization = Const.auth_approved
                msgEd.authorization = Const.auth_approved
            } else {
                root.authorization = Const.auth_unknown
                msgEd.authorization = Const.auth_unknown
            }
            updateState()
            msgEd.updateState()
        }
    }
}


