import QtGraphicalEffects 1.0
import QtQuick 2.7
import QtQuick.Controls 2.4
import QtQuick.Controls.Material 2.0
import QtQuick.Layouts 1.3

import "../opaque" as Opaque
import "../opaque/theme"

ColumnLayout {
    id: root

    MouseArea {
        anchors.fill: parent

        onClicked: {
            forceActiveFocus()
        }
    }

    Opaque.Flickable { // Profile List
        id: sv
        Layout.minimumHeight: 100
        Layout.fillHeight: true
        Layout.minimumWidth: parent.width
        Layout.maximumWidth: parent.width
        contentWidth: colContacts.width
        contentHeight: colContacts.height


        ColumnLayout {
            id: colContacts
            width: sv.width
            spacing: 0

            Connections { // ADD/REMOVE CONTACT ENTRIES
                target: gcd

                onAddProfile: function(handle, displayName, image, tag, unread, online) {

                    // don't add duplicates
                    for (var i = 0; i < profilesModel.count; i++) {
                        if (profilesModel.get(i)["_handle"] == handle) {
                            return
                        }
                    }

                    // find index for insert (sort by onion)
                    var index = profilesModel.count
                    for (var i = 0; i < profilesModel.count; i++) {
                        if (profilesModel.get(i)["_handle"] > handle) {
                            index = i
                            break
                        }
                    }

                    profilesModel.insert(index,
                    {
                        _handle: handle,
                        _displayName: displayName,
                        _image: image,
                        _tag: tag,
                        _online: online,
                        _unread: unread,
                    })
                }

                /*
                onRemoveProfile: function(handle) {
                for(var i = 0; i < profilesModel.count; i++){
                if(profilesModel.get(i)["_handle"] == handle) {
                console.log("deleting contact " + profilesModel.get(i)["_handle"])
                profilesModel.remove(i)
                return
                }
                }
                }*/

                onResetProfileList: function() {
                    profilesModel.clear()
                }
            }

            ListModel { // Profile OBJECTS ARE STORED HERE ...
                id: profilesModel
            }

            Repeater {
                id: profileList
                model: profilesModel // ... AND DISPLAYED HERE
                delegate: ProfileRow {
                    handle: _handle
                    displayName: _displayName
                    image: _image
                    tag: _tag
                    unread: _unread
                    Layout.fillWidth: true
                    profileOnline: _online
                    rowClicked: function(handle) {
                        gcd.broadcast("ResetMessagePane");
                        gcd.broadcast("ResetProfile");
                        gcd.selectedProfile = handle;
                        gcd.loadProfile(handle);
                        parentStack.pane = parentStack.profilePane;
                    }
                    editClicked: function(handle, displayName, tag, image) {
                        profileAddEditPane.load(handle, displayName, tag, image);
                        parentStack.pane = parentStack.addEditProfilePane;
                    }
                }
            }

            Opaque.PortraitRow {
                Layout.fillWidth: true
                handle: ""
                displayName: qsTr("add-new-profile-btn")
                nameColor: Theme.mainTextColor
                image: "/core/account_circle-24px_negative_space.webp"
                tag: ""
                portraitBorderColor: Theme.defaultButtonColor
                portraitColor: Theme.defaultButtonColor
                portraitOverlayColor: Theme.defaultButtonTextColor
                portraitPerformTransform: true
                badgeVisible: true
                badgeContent: Image {
                    source: gcd.assetPath + "core/fontawesome/solid/plus.webp"
                    height: Theme.badgeTextSize * gcd.themeScale
                    width: height
                }
                badgeColor: Theme.portraitProfileBadgeColor

                onClicked: function(handle) { profileAddEditPane.reset(); parentStack.pane = parentStack.addEditProfilePane }
            }


        }
    }
}
