import QtGraphicalEffects 1.0
import QtQuick 2.7
import QtQuick.Controls 2.4
import QtQuick.Controls.Material 2.0
import QtQuick.Layouts 1.3
import CustomQmlTypes 1.0
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4

import "../opaque" as Opaque
import "../opaque/styles"
import "../opaque/theme"
import "../const"


RowLayout {
    id: root
    property alias handle: prow.handle
    property alias displayName: prow.displayName
    property alias image: prow.image
    property alias tag: prow.tag
    property alias badgeColor: prow.badgeColor
    property var rowClicked: {}
    property var editClicked: {}
    property bool profileOnline: false
    property int unread: 0

    Opaque.PortraitRow {
        id: prow
        badgeColor: Theme.portraitProfileBadgeColor
        Layout.fillWidth: true
        portraitBorderColor: Theme.portraitOnlineBorderColor
        portraitColor: Theme.portraitOnlineBackgroundColor
        nameColor: Theme.portraitOnlineTextColor
        onionColor: Theme.portraitOnlineTextColor

        badgeContent: Opaque.Icon {// Profle Type
            id: profiletype
            source: tag == "v1-userPassword" ? gcd.assetPath + "core/lock-24px.webp" : gcd.assetPath + "core/lock_open-24px.webp"
            height: Theme.badgeTextSize * gcd.themeScale
            width: height

            iconColor: Theme.defaultButtonTextColor
            backgroundColor: prow.badgeColor
            hilightBackgroundColor: prow.badgeColor
        }

        Opaque.Badge {
            id: unreadBadge
            visible: unread > 0
            color: Theme.portraitContactBadgeColor
            size: parent.height/4

            anchors.right: parent.right
            anchors.rightMargin: 25 * gcd.themeScale
            anchors.leftMargin: 1 * gcd.themeScale
            anchors.verticalCenter: parent.verticalCenter

            content: Label {
                id: lblUnread
                color: Theme.portraitContactBadgeTextColor
                font.pixelSize: Theme.badgeTextSize * gcd.themeScale
                font.weight: Font.Bold
                text: unread > 99 ? "99+" : unread
            }
        }


        onClicked: { unread = 0; rowClicked(handle) }

        function updateStatus() {
            if (gcd.torStatus != Const.statusOnline) { // Tor network offline
                portraitBorderColor = Theme.portraitOfflineBorderColor
                portraitColor = Theme.portraitOfflineBackgroundColor
                nameColor = Theme.portraitOfflineTextColor
                onionColor = Theme.portraitOfflineTextColor
            } else {
                // TODO: update to include logic on if a peer wants to be online or not (not implemented)

                if (profileOnline == false) {
                    portraitBorderColor = Theme.portraitConnectingBorderColor
                    portraitColor = Theme.portraitConnectingBackgroundColor
                    nameColor = Theme.portraitConnectingTextColor
                    onionColor = Theme.portraitConnectingTextColor
                } else {
                    portraitBorderColor = Theme.portraitOnlineBorderColor
                    portraitColor = Theme.portraitOnlineBackgroundColor
                    nameColor = Theme.portraitOnlineTextColor
                    onionColor = Theme.portraitOnlineTextColor
                }
            }
        }

        Component.onCompleted: { prow.updateStatus() }

        Connections {
            target: gcd

            onTorStatusChanged: function() {
                prow.updateStatus()
            }

            onUpdateProfileNetworkStatus: function(onion, online) {
                if (handle == onion) {
                    profileOnline = online
                    prow.updateStatus()
                }
            }

             onIncContactMessageCount: function(_profile, _handle) {
                if (_profile == handle) {
                    unread++
                }
            }
        }

        Connections {
            target: Theme

            onThemeChanged: {
                prow.updateStatus()
            }
        }
    }

    Opaque.Icon {// Edit BUTTON
        id: btnEdit
        source: gcd.assetPath + "core/edit-24px.webp"
        Layout.minimumWidth: 80
        Layout.fillHeight: true
        backgroundColor: Theme.backgroundMainColor
        hilightBackgroundColor: Theme.backgroundHilightElementColor
        iconColor: Theme.altTextColor

        anchors.verticalCenter: parent.verticalCenter
        // Layout.alignment: Qt.AlignVCenter


        height: root.height / 2
        width: root.height / 2
        size: root.height / 2

        onClicked: editClicked(handle, displayName, tag, image)
    }




}

