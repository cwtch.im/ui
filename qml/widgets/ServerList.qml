import QtGraphicalEffects 1.0
import QtQuick 2.7
import QtQuick.Controls 2.4
import QtQuick.Controls.Material 2.0
import QtQuick.Layouts 1.3

import "../opaque" as Opaque
import "../opaque/theme"

ColumnLayout {
    id: root

    MouseArea {
        anchors.fill: parent

        onClicked: {
            forceActiveFocus()
        }
    }

    Opaque.Flickable { // Profile List
        id: sv
        Layout.minimumHeight: 100
        Layout.fillHeight: true
        Layout.minimumWidth: parent.width
        Layout.maximumWidth: parent.width
        contentWidth: colContacts.width
        contentHeight: colContacts.height


        ColumnLayout {
            id: colContacts
            width: sv.width
            spacing: 0

            Connections { // ADD/REMOVE CONTACT ENTRIES
                target: gcd

                onLoaded: function() {
                    gcd.requestServers();
                }

                onAddServer: function(handle, displayName, image, status, autostart, bundle, messages, key_types, keys) {

                    // don't add duplicates
                    for (var i = 0; i < serversModel.count; i++) {
                        if (serversModel.get(i)["_handle"] == handle) {
                            serversModel.get(i)["_status"] = status
                            serversModel.get(i)["_messages"] = messages
                            return
                        }
                    }

                    // find index for insert (sort by onion)
                    var index = serversModel.count
                    for (var i = 0; i < serversModel.count; i++) {
                        if (serversModel.get(i)["_handle"] > handle) {
                            index = i
                            break
                        }
                    }

                    serversModel.insert(index,
                    {
                        _handle: handle,
                        _displayName: displayName,
                        _image: image,
                        _status: status,
                        _bundle: bundle,
                        _autostart: autostart,
                        _messages: messages
                    })
                }

                onResetServerList: function() {
                    serversModel.clear()
                }
            }

            ListModel { // Profile OBJECTS ARE STORED HERE ...
                id: serversModel
            }

            Repeater {
                id: serverList
                model: serversModel // ... AND DISPLAYED HERE
                delegate: ServerRow {
                    handle: _handle
                    displayName: _displayName
                    image: _image
                    status: _status
                    bundle: _bundle
                    autostart: _autostart
                    messages: _messages
                    Layout.fillWidth: true
                    rowClicked: function(handle) {

                    }
                    editClicked: function(handle, displayName, tag, image) {
                        gcd.checkServer(handle)
                        serverAddEditPane.load(handle, displayName, status, _autostart, _messages, _bundle)
                        parentStack.pane = parentStack.addEditServerPane
                    }
                }
            }

            Opaque.PortraitRow {
                Layout.fillWidth: true
                handle: ""
                displayName: qsTr("add-new-profile-btn")
                nameColor: Theme.mainTextColor
                image: "core/fontawesome/regular/user.webp"
                tag: ""
                portraitBorderColor: Theme.portraitOnlineBorderColor
                portraitColor: Theme.portraitOnlineBackgroundColor
                badgeVisible: true
                badgeContent: Image {
                    source: gcd.assetPath + "core/fontawesome/solid/plus.webp"
                    height: Theme.badgeTextSize * gcd.themeScale
                    width: height
                }
                badgeColor: Theme.defaultButtonColor

                onClicked: function(handle) { gcd.newServer() }
            }
        }
    }



}
