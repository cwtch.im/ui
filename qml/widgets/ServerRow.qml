import QtGraphicalEffects 1.0
import QtQuick 2.7
import QtQuick.Controls 2.4
import QtQuick.Controls.Material 2.0
import QtQuick.Layouts 1.3
import CustomQmlTypes 1.0
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4

import "../opaque" as Opaque
import "../opaque/styles"
import "../opaque/theme"

ProfileRow {
    id: root
    property int status
    property string bundle
    property bool autostart
    property int messages

    badgeColor: status == 1 ? Theme.portraitOnlineBadgeColor : Theme.portraitOfflineBadgeColor
    // TODO Badge Images
}
