import QtQuick 2.7
import QtQuick.Controls 2.4

import "../opaque" as Opaque
import "../opaque/theme"
import "../const"


// Statusbar is a app wide 10-25 tall bar that should be place at the bottom of the app that gives network health information
//   it changes color and text/icon message based on network health. when netowrk is not healthy it is always in fullsized mode
//   when network is health it reduces to a minimal color strip unless mouse overed / clicked to reveal the text/icons
Rectangle {
    id: statusbar

    readonly property int openHeight: 25
    readonly property int hideHeight: 10

    property bool isHover: false

    height: openHeight
    anchors.bottom: parent.bottom
    anchors.left: parent.left
    anchors.right: parent.right

    Text {
        id: statusMessage
        opacity: 0
        anchors.right: networkStatus.left
        anchors.verticalCenter: parent.verticalCenter
        anchors.rightMargin: 5 * gcd.themeScale

        font.pixelSize: Theme.statusTextSize * gcd.themeScale
    }

    Opaque.Icon {
        id: networkStatus
        opacity: 0
        anchors.right: connectionStatus.left
        anchors.verticalCenter: parent.verticalCenter
        anchors.rightMargin: 5 * gcd.themeScale
        height: 18
        width: 18

    }

    Opaque.Icon {
        id: connectionStatus
        opacity: 0
        anchors.right: parent.right
        anchors.verticalCenter: parent.verticalCenter
        anchors.rightMargin: 10 * gcd.themeScale
        height: 18 * gcd.themeScale
        width: 18  * gcd.themeScale
    }

    function changeStatus() {
        if (gcd.torStatus == Const.statusDisconnectedInternet) {
            statusbar.color = Theme.statusbarDisconnectedInternetColor
            statusMessage.color = Theme.statusbarDisconnectedInternetFontColor
            networkStatus.iconColor = Theme.statusbarDisconnectedInternetFontColor
            networkStatus.source = gcd.assetPath + "core/signal_cellular_off-24px.webp"
            connectionStatus.iconColor = Theme.statusbarDisconnectedInternetFontColor
            connectionStatus.source = gcd.assetPath + "core/syncing-03.webp"
            //: Disconnected from the internet, check your connection
            statusMessage.text = qsTr("network-status-disconnected")
            show()
        } else if (gcd.torStatus == Const.statusDisconnectedTor) {
            statusbar.color = Theme.statusbarDisconnectedTorColor
            statusMessage.color = Theme.statusbarDisconnectedTorFontColor
            networkStatus.iconColor = Theme.statusbarDisconnectedTorFontColor
            networkStatus.source = gcd.assetPath + "core/signal_cellular_connected_no_internet_4_bar-24px.webp"
            connectionStatus.iconColor = Theme.statusbarDisconnectedTorFontColor
            connectionStatus.source = gcd.assetPath + "core/syncing-03.webp"
            //: Attempting to connect to Tor network
            statusMessage.text = qsTr("network-status-attempting-tor")
            show()
        } else if (gcd.torStatus == Const.statusConnecting) {
            statusbar.color = Theme.statusbarConnectingColor
            statusMessage.color = Theme.statusbarConnectingFontColor
            networkStatus.iconColor = Theme.statusbarConnectingFontColor
            networkStatus.source = gcd.assetPath + "core/signal_cellular_connected_no_internet_4_bar-24px.webp"
            connectionStatus.iconColor = Theme.statusbarConnectingFontColor
            connectionStatus.source = gcd.assetPath + "core/syncing-02.webp"
            //: Connecting...
            statusMessage.text = qsTr("network-status-connecting")
            show()
        } else {
            statusbar.color = Theme.statusbarOnlineColor
            statusMessage.color = Theme.statusbarOnlineFontColor
            networkStatus.iconColor = Theme.statusbarOnlineFontColor
            networkStatus.source = gcd.assetPath + "core/signal_cellular_4_bar-24px.webp"
            connectionStatus.iconColor = Theme.statusbarOnlineFontColor
            connectionStatus.source = gcd.assetPath + "core/syncing-01.webp"
            //: Online
            statusMessage.text = qsTr("network-status-online")
            hide()
        }
    }

    MouseArea {
        id: ma
        anchors.fill: parent
        hoverEnabled: true

        SequentialAnimation {
            id: showAnim
            PropertyAnimation { id: openStatus; target: statusbar; property: "height"; to: openHeight}
            ParallelAnimation {
                PropertyAnimation { id: showStatus; target: statusMessage; property: "opacity"; to: 1}
                PropertyAnimation { id: showNetIcon; target: networkStatus; property: "opacity"; to: 1}
                PropertyAnimation { id: showConnIcon; target: connectionStatus; property: "opacity"; to: 1}
            }
        }

        SequentialAnimation {
            id: hideAnim
            ParallelAnimation {
                PropertyAnimation { id: hideStatus; target: statusMessage; property: "opacity"; to: 0}
                PropertyAnimation { id: hideNetIcon; target: networkStatus; property: "opacity"; to: 0}
                PropertyAnimation { id: hideConnIcon; target: connectionStatus; property: "opacity"; to: 0}
            }
            PropertyAnimation { id: closeStatus; target: statusbar; property: "height"; to: hideHeight; duration: 200 }
        }

        onEntered: {
            isHover = true
            show()
        }


        onExited: {
            isHover = false
            hide()
        }

        onPressed: {
            isHover = true
            show()
        }

        onReleased: {
            isHover = false
            hide()
        }
    }

    function resetHeight() {
        if (isHover || gcd.torStatus != Const.statusOnline) {
            height = openHeight
        } else {
            height = hideHeight
        }
    }

    function show() {
        if (isHover || gcd.torStatus != Const.statusOnline) {
            hideAnim.stop()
            showAnim.start()
        }
    }

    function hide() {
        if (!isHover && gcd.torStatus == Const.statusOnline) {
            showAnim.stop()
            hideAnim.start()
        }
    }

    Component.onCompleted: { resetHeight(); changeStatus() }

    Connections {
        target: gcd

        onTorStatusChanged: function() {
            changeStatus()
        }
    }

    Connections {
        target: Theme

        onThemeChanged: {
            changeStatus()
        }
    }
}
