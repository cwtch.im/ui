#!/bin/sh

# go get git.openprivacy.ca/openprivacy/qmlfmt

cd qml
find -iname "*.qml" | xargs qmlfmt

cd ..
